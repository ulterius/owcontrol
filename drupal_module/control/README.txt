CONTROL owfs 
DESCRIPTION
-----------

The control module is for administring the 1wire owfs devices, used by the
control.pl script

INSTALLATION
-----------
1) Copy control directory to your modules directory
2) Enable the module at the module administration page
4) Create the external mysql database
5) Add the tables with the control.sql script 
6) Fill in the external db name (beaglebone in this example)  in 'Configuration -> Control external
database' 
