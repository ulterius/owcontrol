-- MySQL dump 10.14  Distrib 5.5.65-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: control
-- ------------------------------------------------------
-- Server version	5.5.65-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `1wdevices`
--

DROP TABLE IF EXISTS `1wdevices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `1wdevices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(128) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `file` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `1wdevices`
--

LOCK TABLES `1wdevices` WRITE;
/*!40000 ALTER TABLE `1wdevices` DISABLE KEYS */;
INSERT INTO `1wdevices` VALUES (3,'20.9F230A000000','DS2450','adc1','volt',''),(17,'20.7D260A000000','DS2450','adc2','volt','ADC 2'),(30,'28.6B57A7020000','DS18B20','fishtank','temperature','Fishtank'),(34,'28.FF3455641403','DS18B20','Greenhouse','temperature',''),(36,'29.CFC922000000','DS2408','pio','PIO',''),(39,'','','Daylight','',''),(40,'26.58F63D020000','DS2438','Humidity','humidity','');
/*!40000 ALTER TABLE `1wdevices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `setvalue` varchar(10) DEFAULT NULL,
  `hystersis` varchar(10) DEFAULT NULL,
  `device` int(11) DEFAULT NULL,
  `devicenr` varchar(10) DEFAULT NULL,
  `iodev` int(11) DEFAULT NULL,
  `iodevnr` varchar(10) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `calibration` varchar(10) DEFAULT NULL,
  `sensortype` int(11) DEFAULT NULL,
  `operation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` VALUES (1,'Daylight','','.2',39,'',NULL,NULL,'','',9,NULL),(2,'Humidity','50.0','0.2',40,'',0,'0','','0.0',3,0),(3,'Waterstorage','10','.2',17,'B',NULL,NULL,'','2.6',1,NULL),(4,'VDD','5','.2',3,'A',NULL,NULL,'','',8,0),(5,'Light','1000','.2',17,'C',NULL,NULL,'','10000',2,0);
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timer`
--

DROP TABLE IF EXISTS `timer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `starttime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `iodev` int(11) DEFAULT NULL,
  `iodevnr` varchar(10) DEFAULT NULL,
  `sensor` int(11) DEFAULT NULL,
  `zone` int(11) DEFAULT NULL,
  `value` varchar(10) DEFAULT NULL,
  `logical` varchar(10) DEFAULT NULL,
  `operation` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timer`
--

LOCK TABLES `timer` WRITE;
/*!40000 ALTER TABLE `timer` DISABLE KEYS */;
INSERT INTO `timer` VALUES (1,'lamp test',3600,79200,36,'3',1,0,'0','>',0,'');
/*!40000 ALTER TABLE `timer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventilation`
--

DROP TABLE IF EXISTS `ventilation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventilation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `zone` int(11) DEFAULT NULL,
  `settemp` varchar(10) DEFAULT NULL,
  `settemp2` varchar(10) DEFAULT NULL,
  `hystersis` varchar(10) DEFAULT NULL,
  `timer` int(11) DEFAULT NULL,
  `timer2` int(11) DEFAULT NULL,
  `iodev` int(11) DEFAULT NULL,
  `iodevnr` int(11) DEFAULT NULL,
  `iodev2` int(11) DEFAULT NULL,
  `iodevnr2` int(10) DEFAULT NULL,
  `operation` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventilation`
--

LOCK TABLES `ventilation` WRITE;
/*!40000 ALTER TABLE `ventilation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventilation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(128) DEFAULT NULL,
  `settemp` varchar(10) DEFAULT NULL,
  `hystersis` varchar(10) DEFAULT NULL,
  `device` int(11) DEFAULT NULL,
  `iodev` int(11) DEFAULT NULL,
  `iodevnr` varchar(10) DEFAULT NULL,
  `iodevsec` int(11) DEFAULT NULL,
  `iodevsecnr` varchar(10) DEFAULT NULL,
  `description` varbinary(256) DEFAULT NULL,
  `iolevelsec` int(11) DEFAULT NULL,
  `operation` int(11) DEFAULT NULL,
  `logical` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='Temperatures';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES (1,'Greenhouse','20','.2',34,36,'3',NULL,NULL,'',NULL,0,'>'),(2,'Fishtank','','.2',30,0,'0',NULL,NULL,'',NULL,0,'>');
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-11 11:50:49
