#!/usr/bin/python
import sys
from pyownet import protocol
import MySQLdb as mdb
import time
import datetime
from astral import Astral
import os
import socket

def dbquery(vars,query,select=''):
	con = mdb.connect(vars["dbhost"], vars["dbuser"], vars["dbpassword"], vars["database"])
	with con:
		cur = con.cursor(mdb.cursors.DictCursor)
	cur.execute(query)
	if select:
		rows = cur.fetchone()
	else:
		rows = cur.fetchall()
        return rows

def owread(vars,device):
        owhosts=vars["owhosts"]
        ows=owhosts.split(",")
        for ow in ows:
            owhost=ow.split(":")
            if vars["debug"]: print owhost[0], owhost[1]
            try:
                proxy = protocol.proxy(owhost[0], owhost[1])
            except (protocol.ConnError, protocol.ProtocolError) as err:
                if vars["debug"]: print(err)
            else:
                try:
                    value = float(proxy.read(device))
                except protocol.OwnetError as err:
                    if vars["debug"]: print(err)
                    value = ''
                else:
                    if vars["debug"]: print "Succeeded reading device: " + str(value)
                    break
        return value

def owwrite(vars,state,row,nr=''):
        owhosts=vars["owhosts"]
        if nr:
            iodevice=str(row["iodevice" + nr]) + "/" + str(row["iofile" + nr]) + "." + str(row["iodevnr" + nr])
        else:
            iodevice=str(row["iodevice"]) + "/" + str(row["iofile"]) + "." + str(row["iodevnr"])
        ows=owhosts.split(",")
        for ow in ows:
            owhost=ow.split(":")
            if vars["debug"]: print owhost[0], owhost[1], iodevice
            try:
                proxy = protocol.proxy(owhost[0], owhost[1])
            except (protocol.ConnError, protocol.ProtocolError) as err:
                print(err)
                result=err
            else:
                try:
                    proxy.write(iodevice, state)
                except protocol.OwnetError:
                    result="couldn\'t write",iodevice,state
                else:
                    result="ok",iodevice,state
                    break
        return result

def daytime_sec():
    now = datetime.datetime.now()
    return int((now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds())

def daylight(vars,row):
    city_name=vars["city_name"]
    daylightcorrection=vars["daylightcorrection"]
    if row["value"]:
        daylightcorrection=float(row["value"])

    current=daytime_sec()

    a = Astral()
    a.solar_depression = 'civil'
    city = a[city_name]
    timezone = city.timezone
    sun = city.sun(date=datetime.datetime.now() , local=True)
    sunrise=str(sun['sunrise']).split('+')[0]
    sunset=str(sun['sunset']).split('+')[0]

    x = time.strptime(sunrise.split(' ')[1],'%H:%M:%S')
    sunrise=datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()

    x = time.strptime(sunset.split(' ')[1],'%H:%M:%S')
    sunset=datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()


    if vars["debug"]: print('Day time')+ str(sunrise) +  str(sunset)
    sunrise += daylightcorrection
    sunset -= daylightcorrection

    if sunrise < current and sunset >current:
        if vars["debug"]: print('Day time')+ str(sunrise) + "<" + str(current) + ">" + str(sunset)
        return True    
    else:
        if vars["debug"]: print('Night time')
        return False

def rrdsave(vars,name,value):
    if vars["rrdhost"] and vars["rrdport"]:
        rrdhost=vars["rrdhost"]
        rrdport=vars["rrdport"]
        rrddbpath=vars["rrddbpath"]
        rrddb=rrddbpath + "/" + name + ".rrd"
        rrddbcreate=vars["rrddbcreate"] + "\n"
        rrddbcreate=rrddbcreate.replace('RRDDB',rrddb)
        error=''
        
        # create socket
        if vars["debug"]: print('# Creating socket')
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            if vars["debug"]: print('Failed to create socket')
            error='Failed to create socket'
        else:
            try:
                remote_ip = socket.gethostbyname( rrdhost )
            except socket.gaierror:
                if vars["debug"]: print('Hostname could not be resolved')
                error='Hostname could not be resolved.'
            else:
                
                # Connect to remote server
                if vars["debug"]: print('# Connecting to server, ' + str(rrdhost) + ' (' + str(remote_ip) + ') port ' + str(rrdport))
                s.connect((remote_ip , rrdport))
                
                # Check if rrd db exits
                if vars["debug"]: print('# Sending data to server')
                request = "info " +  rrddb  + "\n"
                
                try:
                    s.sendall(request)
                except socket.error:
                    if vars["debug"]: print 'Send failed'
                    error='Send failed'
                else:
                    
                    # Receive data
                    if vars["debug"]: print('# Receive data from server')
                    reply = s.recv(4096)
                    
                    #if reply.find("OK"):
                    if not "OK" in reply:
                        if "No such file or directory" in reply:
                            #Try to create the rrd db.
                            if vars["debug"]: print "Not found" + rrddb + " try creating"
                            try:
                                s.sendall(rrddbcreate)
                            except socket.error:
                                if vars["debug"]: print 'Unable to create db ' + rrddb
                                error='Unable to create rrd db ' + rrddb
                            else:
                                reply = s.recv(4096)
                                if "ERROR" in reply:
                                    out='rrd db create failed' + reply + "\n"
                                else:
                                    out='rrd create ok' + reply + "\n"
        if not error:
            if vars["debug"]: print "Everthing looks fine, try updating db"
            request = "update " +  rrddb  + " N:" +  str(value) + "\n"
            try:
                s.sendall(request)
            except socket.error:
                if vars["debug"]: print 'Send failed'
                out='Send failed ' + request
            else:
                out = s.recv(4096)
        else:
            return error
        return out

def rrdimage(vars,rrdpng):
    if vars["rrdhost"] and vars["rrdport"]:
        rrdhost=vars["rrdhost"]
        rrdport=vars["rrdport"]
        # create socket
        if vars["debug"]: print('# Creating socket')
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            if vars["debug"]: print('Failed to create socket')
            error='Failed to create socket\n'
        else:
            try:
                remote_ip = socket.gethostbyname( rrdhost )
            except socket.gaierror:
                if vars["debug"]: print('Hostname could not be resolved')
                error='Hostname could not be resolved.\n'
            else:
                
                # Connect to remote server
                if vars["debug"]: print('# Connecting to server, ' + str(rrdhost) + ' (' + str(remote_ip) + ') port ' + str(rrdport))
                s.connect((remote_ip , rrdport))
                
                try:
                    s.sendall(rrdpng)
                except socket.error:
                    if vars["debug"]: print 'Send failed'
                    out='Send failed ' + rrdpng
                else:
                    reply = s.recv(4096)
                    if "ERROR" in reply:
                        if vars["debug"]: print('Image create failed' + reply + "\n")
                        out=''
                    else:
                        out='Image create ok'
    return out



#Measuring and controlling temperatures
def temperature_zones(vars):
	vars["tempzones"]={}
	vars["tempcontrolzones"]={}
        vars["temprrdimages"]=[]
        if vars["debug"]: print "<<<<<<< TEMPERATURE >>>>>>>";

        query="SELECT z.id AS id, z.zone AS zone, d.device AS device, d.file AS file, z.settemp AS settemp, z.hystersis AS hystersis, z.logical as logical,d2.device AS iodevice, z.iodevnr AS iodevnr, d2.file AS iofile, z.operation AS operation, z.description AS description FROM zone z LEFT OUTER JOIN 1wdevices d ON z.device=d.id LEFT OUTER JOIN 1wdevices d2 ON z.iodev=d2.id"
        rows=dbquery(vars,query)
	for row in rows:
            temp=owread(vars,row["device"] + '/' + row["file"])
            if temp:
                if vars["debug"]: print row["id"], row["zone"], temp
                id=int(row["id"])
                vars["tempzones"][id]=temp
		vars["tempcontrolzones"][id]=vars["off"]
                #RRD
                #rrd save
                if vars["rrd"]:
                    result=rrdsave(vars,"temperature_" + str(id),temp)
                    if vars["debug"]: print "rrd result" + str(result)
                    #rrd image
                    rrdpng=vars["rrdimage"] + "\n"
                    image=vars["rrdimagepath"]+row["zone"].replace(" ","-")+".png"
                    rrdpng=rrdpng.replace("IMAGE",image)
                    rrdpng=rrdpng.replace("OPTS","")
                    rrdpng=rrdpng.replace("DESCRIPTION",row["zone"]+" "+str(datetimecustom()))
                    rrdpng=rrdpng.replace("GRAPH",row["zone"])
                    rrdpng=rrdpng.replace("SENSORTYPE", "temperature")
                    rrdpng=rrdpng.replace("RRDDB",vars["rrddbpath"]+"temperature_" + str(id)+".rrd")
                    result=rrdimage(vars,rrdpng)
                    if result:
                        vars["temprrdimages"].append(os.path.basename(image))
                    if vars["debug"]: print "rrd image result: " + str(result)

                if not  row["logical"]:
                    row["logical"]='>'
                # operation == 2 control the temperature
                if row["operation"] == 2:
                    if row["settemp"]:
                        if row["hystersis"]:
                            hystersis=float(row["hystersis"])/2
                        else:
                            hystersis=float(1)/2
                        temp_set_upper=float(row["settemp"]) + float(hystersis)
                        temp_set_lower=float(row["settemp"]) - float(hystersis)
                        if temp < temp_set_lower or temp > temp_set_upper:

                            if row["logical"] == ">":
                                if temp > temp_set_lower:
                                    if vars["debug"]: print "heating...", temp_set_upper
                                    vars["tempcontrolzones"][id]=vars["on"]
                                if temp < temp_set_upper:
                                    if vars["debug"]: print "stop heating...", temp_set_lower
                                    vars["tempcontrolzones"][id]=vars["off"]
                            if row["logical"] == "<":
                                if temp < temp_set_lower:
                                    if vars["debug"]: print "cooling...", temp_set_upper
                                    vars["tempcontrolzones"][id]=vars["on"]
                                if temp > temp_set_upper:
                                    if vars["debug"]: print "stop cooling...", temp_set_lower
                                    vars["tempcontrolzones"][id]=vars["off"]

                elif row["operation"] == 1:
                     if vars["debug"]: print "heating, allways on."
                     vars["tempcontrolzones"][id]=vars["on"]
                elif row["operation"] == 0:
                     if vars["debug"]: print "heating, allways off."
                     vars["tempcontrolzones"][id]=vars["off"]
                else:
                     if vars["debug"]: print "heating, allways off."
                     vars["tempcontrolzones"][id]=vars["off"]
                if row["iodevice"]:
                    result=owwrite(vars,vars["tempcontrolzones"][id],row)
                    if vars["debug"]: print result
            else:
                 if vars["debug"]: print row["id"], row["zone"], 'no value'
	return(vars)

def timer_zones(vars):

    vars["timercontrolzones"]={}
    if vars["debug"]: print "<<<<<<< TIMER >>>>>>>";
    query="SELECT t.id AS id, t.name AS name, t.starttime AS starttime, t.endtime AS endtime, t.logical as logical, d.name AS namedev, d.device AS iodevice, d.file AS iofile, t.iodevnr AS iodevnr, t.sensor AS sensor, t.zone AS zone, s.name AS sensorname, s.sensortype AS sensortype, z.zone AS zonename, t.value AS value, t.operation AS operation, t.description AS description FROM timer t LEFT OUTER JOIN 1wdevices d ON t.iodev=d.id LEFT OUTER JOIN sensor s ON t.sensor=s.id LEFT OUTER JOIN zone z ON t.zone=z.id"
    rows=dbquery(vars,query,False)
    for row in rows:
        if vars["debug"]: print "row:",row
        id=int(row["id"])
        sensortype=int(row["sensortype"])
        if row["sensor"] and sensortype > 0 and sensortype < 9:
            sensorid=int(row["sensor"])
            value=float(vars["sensorzones"][sensorid])
            setvalue=float(row["value"])
            sensorname=row["sensorname"]
        if row["zone"]:
            zoneid=int(row["zone"])
            value=float(vars["tempzones"][zoneid])
            setvalue=float(row["value"])
            sensorname=row["zonename"]
	operation=int(row["operation"])
	vars["timercontrolzones"][id]=vars["off"]
        day_sec=daytime_sec()
        if operation == 2 and sensortype:
	    if vars["debug"]: print "auto operation"
            if day_sec > row["starttime"] and day_sec < row["endtime"]:
                if sensortype > 0 and sensortype < 9 or row["zone"]:
                    if vars["debug"]: print "if " , sensorname, str(value) , row["logical"] , str(setvalue)
                    if row["logical"] == ">":
                       if value > setvalue :
                          vars["timercontrolzones"][id]=vars["on"]
                       else:
                          vars["timercontrolzones"][id]=vars["off"]
                    if row["logical"] == "<":
                       if value < setvalue :
                          vars["timercontrolzones"][id]=vars["on"]
                       else:
                          vars["timercontrolzones"][id]=vars["off"]
                    else:
                       vars["timercontrolzones"][id]=vars["off"]
                elif sensortype == 9:
                        if vars["debug"]: print "daylight?"
                        if daylight(vars,row):
                            vars["timercontrolzones"][id]=vars["off"]
                        else:
                            vars["timercontrolzones"][id]=vars["on"]
                else:
                    if vars["debug"]: print "timer", row["name"] ,"on"
                    vars["timercontrolzones"][id]=vars["on"]

        elif operation == 1:
            if vars["debug"]: print "Allways on.",row["name"]
            vars["timercontrolzones"][id]=vars["on"]
        elif operation == 0:
            if vars["debug"]: print "Allways off.",row["name"]
            vars["timercontrolzones"][id]=vars["off"]
        else:
            if vars["debug"]: print "Allways off.",row["name"]
            vars["timercontrolzones"][id]=vars["off"]
        if row["iodevice"] and vars["timercontrolzones"][id]:
            result=owwrite(vars,vars["timercontrolzones"][id],row)
            if vars["debug"]: print result
	else:
            if vars["debug"]: print "No iodevice or timercontrolzones set"
    return(vars)

def sensor_zones(vars):
                
#    '0'  => '',
#    '1' => t('waterstorage'),
#    '2' => t('light'),
#    '3' => t('humidity'),
#    '8' => t('VDD'),
#    '9' => t('sunrise/sunset'),
    vars["sensorzones"]={}
    vars["sensorcontrolzones"]={}
    vars["sensorrrdimages"]=[]
    if vars["debug"]: print "<<<<<<< SENSORS >>>>>>>";
    query="SELECT s.id AS id, s.name AS name, s.setvalue AS setvalue, s.sensortype AS sensortype, s.hystersis AS hystersis, s.calibration AS calibration, s.devicenr AS devicenr, s.iodevnr AS iodevnr, s.operation AS operation, s.description AS description, d.name AS namedevice, d.device AS device, d.file AS devicefile, d2.name AS ioname, d2.device AS iodevice, d2.file AS iofile FROM sensor s LEFT OUTER JOIN 1wdevices d ON s.device=d.id LEFT OUTER JOIN 1wdevices d2 ON s.iodev=d2.id"
    rows=dbquery(vars,query)
    for row in rows:
        if vars["debug"]: print row
        value = None
        if row["device"]:
            if row["devicenr"]:
                if vars["debug"]: print row["device"] + '/' + row["devicefile"] + '.' + row["devicenr"]
                value=owread(vars,row["device"] + '/' + row["devicefile"] + '.' + row["devicenr"])
            else:
                value=owread(vars,row["device"] + '/' + row["devicefile"])
        if value:
            if vars["debug"]: print "Value sensor: "  + str(value)
            if rrd:
                rrdpng=vars["rrdimage"] + "\n"
            if row["sensortype"] == 1:
                #Waterstorage, done with a potmeter
                #Try reading the powersupply voltage of the DS2438 chip.
                VDD=owread(vars,row["device"] + '/VDD')
                if not isinstance(VDD, float):
                    VDD=vars["vdd"]
                if vars["debug"]: print "VDD: ", VDD
                #Calculate percentage of the level, calibrating factor is row["calibration"]
                if row["calibration"]:
                   value=(float(row["calibration"])*(value / float(VDD))) * 100
                value=format(value, '.0f')
                vars["sensorzones"][row["id"]]=value
                if vars["debug"]: print "Value Water storage: " + value
                if rrd:
                    #Save in rrd
                    result=rrdsave(vars,"sensor_" + str(row["id"]),value)
                    if vars["debug"]: print "rrd result " + result
                    rrdpng=rrdpng.replace("OPTS","-u 100 -l 0 -s end-1week")
            elif row["sensortype"] == 2:
                #(Sun) Light  in lux, done with mini solar cell, lux calibrating factor is row["calibration"]
                if row["calibration"]:
                   value=float(row["calibration"])*float(value) 
                value=format(value, '.0f')
                vars["sensorzones"][row["id"]]=value
                if vars["debug"]: print "Light: " + str(value)
                if rrd:
                    #Save in rrd
                    result=rrdsave(vars,"sensor_" + str(row["id"]),value)
                    if vars["debug"]: print "rrd result " + result
                    rrdpng=rrdpng.replace("OPTS","-o --units=si  -r -u 100000 -l 100")
            elif row["sensortype"] == 3:
                #Humidity, done with ds2438 and a HIH device, see: https://onewirefilesystem.org/humidity
                if row["calibration"]:
                   value=float(row["calibration"])*float(value) 
                value=format(value, '.0f')
                vars["sensorzones"][row["id"]]=value
                if rrd:
                    #Save in rrd
                    result=rrdsave(vars,"sensor_" + str(row["id"]),value)
                    if vars["debug"]: print "rrd result " + result
                    rrdpng=rrdpng.replace("OPTS","-u 100 -l 0")
            elif row["sensortype"] == 8:
                vars["vdd"]=value
            if rrd:
                rrdpng=rrdpng.replace("OPTS","")
                image=vars["rrdimagepath"]+row["name"].replace(" ","-")+".png"
                rrdpng=rrdpng.replace("IMAGE",image)
                rrdpng=rrdpng.replace("DESCRIPTION",row["name"]+" "+str(datetimecustom()))
                rrdpng=rrdpng.replace("GRAPH",row["name"])
                rrdpng=rrdpng.replace("SENSORTYPE", "sensor")
                rrdpng=rrdpng.replace("RRDDB",vars["rrddbpath"]+"sensor_" + str(row["id"])+".rrd")
                result=rrdimage(vars,rrdpng)
                if result:
                   vars["sensorrrdimages"].append(os.path.basename(image))
            #controlling IO
            vars["sensorcontrolzones"][row["id"]]=vars["off"]
            if row["setvalue"] and value:
                if row["hystersis"]:
                    hystersis=float(row["hystersis"])/2
                else:
                    hystersis=float(1)/2
                setvalue_upper=float(row["setvalue"]) + float(hystersis)
                setvalue_lower=float(row["setvalue"]) - float(hystersis)
                value=float(value)
                if row["operation"] == 2:
                     if vars["debug"]: print "sensor controlled, auto."
                     if value < setvalue_lower or value > setvalue_upper:
                          if value < setvalue_lower:
                               if vars["debug"]: print "sensor controlled on...",  setvalue_lower
                               vars["sensorcontrolzones"][row["id"]]=vars["on"]
                          if value > setvalue_upper:
                               if vars["debug"]: print "sensor controlled off...", value, setvalue_upper
                               vars["sensorcontrolzones"][row["id"]]=vars["off"]
                elif row["operation"] == 1:
                     if vars["debug"]: print "sensor controlled, allways on."
                     vars["sensorcontrolzones"][row["id"]]=vars["on"]
                elif row["operation"] == 0:
                     if vars["debug"]: print "sensor controlled, allways off."
                     vars["sensorcontrolzones"][row["id"]]=vars["off"]
                if row["iodevice"] and vars["sensorcontrolzones"][row["id"]]:
                     result=owwrite(vars,vars["sensorcontrolzones"][row["id"]],row)
                     if vars["debug"]: print result
	        else:
                     if vars["debug"]: print "sensorcontrolzones set"
    return(vars)

def ventilation_zones(vars):
    #Ventilation, window opener.  Position of the window based on time, seconds.

    query="SELECT v.id AS id, v.name AS name, z.zone AS zone,z.id as zoneid, v.settemp AS settemp, v.hystersis AS hystersis, d.name AS namedev, d.device AS iodevice, d.file AS iofile, d2.name AS namedev2, d2.device AS iodevice2, d2.file AS iofile2, v.iodevnr AS iodevnr, v.iodevnr2 AS iodevnr2, v.timer AS timer, v.timer2 AS timer2, v.settemp2 AS settemp2, v.operation AS operation, v.description AS description FROM ventilation v LEFT OUTER JOIN zone z ON v.zone=z.id LEFT OUTER JOIN 1wdevices d ON v.iodev=d.id LEFT OUTER JOIN 1wdevices d2 ON v.iodev2=d2.id"
    rows=dbquery(vars,query,False)
    for row in rows:
        if vars["debug"]: print "row:",row
        id=int(row["id"])
        temp=vars["tempzones"][id]
        #check if array exists
        try:
            _= vars["ventilationcontrolzones"][id]
        except KeyError:
            vars["ventilationcontrolzones"]={}
        # operation == 2 control the temperature
        if row["operation"] == 2:
            if row["settemp"]:
               if row["hystersis"]:
                   hystersis=float(row["hystersis"])/2
               else:
                   hystersis=float(2)/2
               temp_set_upper=float(row["settemp"]) + float(hystersis)
               temp_set_lower=float(row["settemp"]) - float(hystersis)
               if row["settemp2"]:
                   temp_set_upper2=float(row["settemp2"]) + float(hystersis)
                   temp_set_lower2=float(row["settemp2"]) - float(hystersis)
                   if (temp < temp_set_lower or temp > temp_set_upper) and temp < temp_set_lower2:
                       if temp > temp_set_upper and temp < temp_set_lower2:
                            if vars["debug"]: print "Open vent...", temp_set_upper
                            vars["ventilationcontrolzones"][id]=vars["on"]
                            step=1
                            iovent(vars,row,step)
                       if temp < temp_set_lower:
                            if vars["debug"]: print "Closing vent...", temp_set_lower
                            vars["ventilationcontrolzones"][id]=vars["off"]
                            step=1
                            iovent(vars,row,step)
                   if (temp < temp_set_lower2 or temp > temp_set_upper2) and temp > temp_set_upper:
                      if temp > temp_set_upper2:
                         if vars["debug"]: print "Open vent step 2...", temp_set_upper2
                         vars["ventilationcontrolzones"][id]=vars["on"]
                         step=2
                         iovent(vars,row,step)
                      if temp < temp_set_lower2 and temp > temp_set_upper:
                         if vars["debug"]: print "Closing vent step 2...", temp_set_lower2
                         vars["ventilationcontrolzones"][id]=vars["off"]
                         step=2
                         iovent(vars,row,step)
               else:
                   if temp < temp_set_lower or temp > temp_set_upper:
                       if temp > temp_set_upper:
                            if vars["debug"]: print "Open vent...", temp_set_upper
                            vars["ventilationcontrolzones"][id]=vars["on"]
                            step=1
                            iovent(vars,row,step)
                       if temp < temp_set_lower:
                            if vars["debug"]: print "Closing vent...", temp_set_lower
                            vars["ventilationcontrolzones"][id]=vars["off"]
                            step=1
                            iovent(vars,row,step)

        elif row["operation"] == 1:
             if vars["debug"]: print "heating, allways on."
             vars["ventilationcontrolzones"][id]=vars["on"]
             step=1
             iovent(vars,row,step)
        elif row["operation"] == 0:
             if vars["debug"]: print "heating, allways off."
             vars["ventilationcontrolzones"][id]=vars["off"]
             step=1
             iovent(vars,row,step)
        else:
             if vars["debug"]: print "heating, allways off."
             vars["ventilationcontrolzones"][id]=vars["off"]
             step=1
             iovent(vars,row,step)
    return(vars)

def iovent(vars,row,step):
    id=int(row["id"])
    if step == 1:
        timer=row["timer"]
    else:
        timer=row["timer2"]
    try:
        _=vars["ventilationcontrolzones"]['status'+str(step)]
    except KeyError:
        vars["ventilationcontrolzones"]['status'+str(step)]=1

    if vars["ventilationcontrolzones"][id]==vars["on"] and vars["ventilationcontrolzones"]['status'+str(step)] != 2:
         result=owwrite(vars,vars["on"],row)
         if vars["debug"]: print result
         if vars["debug"]: print "Wait..." + str(timer)
         time.sleep( timer )
         result=owwrite(vars,vars["off"],row)
         if vars["debug"]: print result
         vars["ventilationcontrolzones"]['status'+str(step)]=2;
    elif vars["ventilationcontrolzones"][id]==vars["off"] and vars["ventilationcontrolzones"]['status'+str(step)] != 1:
         result=owwrite(vars,vars["on"],row,str('2'))
         if vars["debug"]: print result
         if vars["debug"]: print "Wait..." + str(timer)
         time.sleep( timer )
         result=owwrite(vars,vars["off"],row,str('2'))
         if vars["debug"]: print result
         vars["ventilationcontrolzones"]['status'+str(step)]=1;

def output(vars):
    #Output of the status html page. A html table and rrdtool images.
    tdclasson="control_on"
    tdclassoff="control_off"
    out=''
    out+='<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr">\n\
<head profile="http://www.w3.org/1999/xhtml/vocab">\n\
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n\
<link rel="stylesheet" type="text/css" href="control.css">\n\
<title>Control</title>\n\
</head>'
    out+="<table>\n";
    out+="<tr><th>Name></th><th>Value</th><th>Set</th></tr>\n";
    if vars["tempzones"]:
      for id,value in vars["tempzones"].iteritems():
        query="SELECT  zone, settemp, description FROM zone where id={} order by zone".format(id)
        row=dbquery(vars,query,True)
        id=int(id)
	value="{0:.1f}".format(value)
	if row["description"]:
	   name=row["description"]
        else:
	   name=row["zone"]
	if vars["tempcontrolzones"][id] and vars["tempcontrolzones"][id] == vars["on"]:
	   tdclass=tdclasson
	else:
	   tdclass=tdclassoff
	out+="<tr><td>{}</td><td class='{}'>{}</td><td>{}</td></tr>\n".format(name,tdclass,value,row["settemp"])

    if vars["sensorzones"]:
      for id,value in vars["sensorzones"].iteritems():
        query="SELECT name, description,setvalue FROM sensor where id={} order by name".format(id)
        row=dbquery(vars,query,True)
        id=int(id)
	if row["description"]:
	   name=row["description"]
        else:
	   name=row["name"]
	if vars["sensorcontrolzones"][id] and vars["sensorcontrolzones"][id] == vars["on"]:
	   tdclass=tdclasson
	else:
	   tdclass=tdclassoff
	out+="<tr><td>{}</td><td class='{}'>{}</td><td>{}</td></tr>\n".format(name,tdclass,value,row["setvalue"])
    out+="</table>\n";

    if  vars["timercontrolzones"]:
      for id in vars["timercontrolzones"]:
        query="SELECT name, description FROM timer where id={} order by name".format(id)
        row=dbquery(vars,query,True)
        id=int(id)
	if row["description"]:
	   name=row["description"]
        else:
	   name=row["name"]
	if vars["timercontrolzones"][id] and vars["timercontrolzones"][id] == vars["on"]:
	   tdclass=tdclasson
	   out+="<div class='{}'>{}</div>".format(tdclass,name)
    if  vars["temprrdimages"]:
      for png in vars["temprrdimages"]:
        out+="<img src={}>\n".format(png)
    if vars["sensorrrdimages"]:
      for png in vars["sensorrrdimages"]:
        out+="<img src={}>\n".format(png)
    try:
       with open(vars["statusfile"], 'w') as f:
            f.write(out)
    except IOError as e:
        if vars["debug"]: print "I/O error({0}): {1}".format(e.errno, e.strerror)
    if vars["debug"]: print out


def strtobin(value):
    if '1' in value:
        return b'1'
    else:
        return b'0'
def datetimecustom():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d %H:%M")

