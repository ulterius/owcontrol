#!/usr/bin/perl
use lib '/usr/share/perl5/DateTime';
use Time::Local;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);
use POSIX qw/strftime/;
use Astro::Sunrise;
use OWNet;
use IO::Socket;
use Net::Ping;
use File::Basename;
use IO::Handle;
STDOUT->autoflush(1);
use DBI;
use DBD::mysql;

use strict;

my $debug=$ENV{DEBUG};
#if ( ! $debug ){
#my $pid = fork;
#exit if $pid;
##die "Couldn't fork: $!" unless defined($pid);
#}


$SIG{INT} = $SIG{TERM} = $SIG{HUP} = \&signal_handler;

my $checkfile;
my $checklight="/dev/shm/licht";
my $nagioscheck="/dev/shm/nagios";
my $db="/mnt/rrd/data";
my $www="/var/www/images";
my $out="/dev/shm/temps.out";
my $statusout="/dev/shm/status.out";
my $errorfile="/dev/shm/error_control.log";
my $statusfile="/var/log/status";
my $logfile="/dev/shm/control.log";
my $dumplock="/dev/shm/dump.lck";
my $hyst=".2";
my $loop="60";
my $day=3600 * 24;
my $hours="3600";
my $low="1";
my $iterationsl=300/$loop;
my $high="2";
my $waterstoragefull=".4";
my $longitude="4.8";
my $latitude="52";
my $timezone="1";
my $off="1";
my $on="0";
my $rrdhost = "rrdhost";
my $rrdport = 13900;
my $rrddbport= 13907;
my $owaddresssrc="localhost:4304 owserver:4304";
my %zoneconfigs;
my %zonestatus;
my %timerstatus;
my $day_sec;
my $log;
my $statuslog;
my $status;
my $ventstatus;
my $text;
my $nagios;
my $deviceserror;
my $islight;
my $isdark;
my $lightzero=.06;
#my $owaddress;
##



while() {
	my $endloop;
	my $time;
	my $startloop=time();
	my $class="control_off";
        print "Start: $startloop  \n" if $debug;
	$text="<table class=\"control\"><tr><th>Zone</th><th>Waarde</th><th>Ingesteld</th></tr>\n";
	undef $status;
	undef $statuslog;
	undef $nagios;
	undef %zonestatus;
	undef %timerstatus;
	undef $deviceserror;
	#$owaddress=check_hosts($owaddresssrc);
	#	owfs_pio_init();
	print "\n>>>>>>>> Temp\n" if $debug;
	my @ids=owfs_temp_init();
	for my $id (@ids){
		if ($zoneconfigs{'zone'}{$id}{'path'}){
			read_sensor_temp($id);
			if ($zoneconfigs{'zone'}{$id}{'value'}){
				$nagios.=$zoneconfigs{'zone'}{$id}{'desc'}.';'.sprintf("%.1f",$zoneconfigs{'zone'}{$id}{'value'}).';'.$zoneconfigs{'zone'}{$id}{'status'}."\n";
				temp_control($id);
				temp_controlsec($id);
				rrd('zone',$id);
			}
        	}

	}
	foreach my $address (keys %zonestatus) {
        	while (my ($device, $value) = each %{ $zonestatus{$address} } ) {
                	print "Temperature req Address $address  $device = $value \n" if $debug;
			if($device){
				writeio($address,$device,$value);
			}
        	}
	}
	print "\n>>>>>>>> Vent\n" if $debug;
	my @ids=owfs_ventilation_init();
	for my $id (@ids){
		vent_control($id);
	}
	print "\n>>>>>>>> Sensor\n" if $debug;
	my @ids=owfs_sensor_init();
	for my $id (@ids){
		read_sensor($id);
	}
	print "\n>>>>>>>> Timer\n" if $debug;
	my @ids=owfs_timer_init();
	for my $id (@ids){
			timer_control($id);
	}
	foreach my $address (keys %timerstatus) {
        	while (my ($device, $value) = each %{ $timerstatus{$address} } ) {
                	print "Timer req Address $address  $device = $value \n" if $debug;
			if($device){
				writeio($address,$device,$value);
			}
        	}
	}
	print_result();

##Wacht voor in totaal $loop sec.
        $endloop=$startloop + $loop;
        if ( $endloop > time() ){
                while ($endloop > time() ){
                        sleep 1;
			$time=$endloop-time();
			print "$time\r" if $debug;
                }
        }else{
		$time=time() - $startloop;
                print "Loop duurt langer dan $loop seconden!\n" if $debug;
		$log=$log . "Loop duurt langer dan $loop seconden!: $time \n";
        }
	my $loopduratain=time() - $startloop;
	print "\nLoop time: $loopduratain\n" if $debug;
	#        lightfilestatus();

}

sub trim
  {
    my $string = shift;
    chomp $string;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
  }
sub check
  {
    my $measure = shift;
    my $orginal = shift;
    chomp $measure;
    chomp $orginal;
    $measure =~ s/^\s+//;
    $measure =~ s/\s+$//;
    if ($measure  < -50 ){
        return($orginal);
     }
    if ($measure  >= "85" ){
        return($orginal);
     }
    return($measure);
  }


sub read_sensor_temp(){
	my $id= shift @_;
	print "Reading $id" if $debug;
	my $valtmp=readval('zone',$id);
	if ($valtmp){
		if($zoneconfigs{'zone'}{$id}{'value'}){
			 $zoneconfigs{'zone'}{$id}{'value'}=check($valtmp,$zoneconfigs{'zone'}{$id}{'value'});
		}else{
			$zoneconfigs{'zone'}{$id}{'value'}=trim($valtmp);
		}
			
	}else{
		save_error(get_time()." Cannot open $zoneconfigs{'zone'}{$id}{'description'} $zoneconfigs{'zone'}{$id}{'path'}\n");
	}
	print "$id $zoneconfigs{'zone'}{$id}{'value'}\n" if $debug;
}

sub read_sensor(){
#               '0'  => '',
#                '1' => t('waterstorage'),
#                '2' => t('light'),
#                '3' => t('sunrise/sunset'),
#                '4' => t('temperature'),
#                '5' => t('humidity'),
#                '9' => t('VDD'),
	my $id= shift @_;
	my $class="control_off";
	my $sensortype=$zoneconfigs{'sensor'}{$id}{'sensortype'};
	print ">>Reading sensor $sensortype \n" if $debug;
	if ($sensortype eq "9"){
		my $valtmp=readval('sensor',$id);
		if ($valtmp){
                        	$zoneconfigs{'sensor'}{$id}{'value'}=$valtmp;
		}else{
			save_error(get_time()." Cannot open $zoneconfigs{'sensor'}{$id}{'description'} $zoneconfigs{'sensor'}{$id}{'path'}\n");
		}
		print ">Reading $sensortype : $zoneconfigs{'sensor'}{$id}{'value'}\n" if $debug;
	}
	elsif ($sensortype eq "4"){
	my $valtmp=readval('sensor',$id);
	if ($valtmp){
		if($zoneconfigs{'sensor'}{$id}{'value'}){
			 $zoneconfigs{'sensor'}{$id}{'value'}=check($valtmp,$zoneconfigs{'sensor'}{$id}{'value'});
		}else{
			$zoneconfigs{'sensor'}{$id}{'value'}=trim($valtmp);
		}
			
	}else{
		save_error(get_time()." Cannot open $zoneconfigs{'sensor'}{$id}{'description'} $zoneconfigs{'sensor'}{$id}{'path'}\n");
	}
	print "$sensortype $zoneconfigs{'sensor'}{$id}{'value'}\n" if $debug;
	}elsif ($sensortype eq "2"){
		my $valtmp=readval('sensor',$id);
		if ($valtmp){
			if($valtmp > 0){
				$valtmp=trim($valtmp)-$lightzero;
                        	$zoneconfigs{'sensor'}{$id}{'value'}=sprintf("%.0f", ($valtmp * 50000));
				if ($zoneconfigs{'sensor'}{$id}{'value'} < 0){
					$zoneconfigs{'sensor'}{$id}{'value'}=0;
				}
                	}else{
                        	$zoneconfigs{'sensor'}{$id}{'value'}=0;
                	}

			my $light=$zoneconfigs{'sensor'}{$id}{'value'};
			my $lightsethigh=$zoneconfigs{'sensor'}{$id}{'set'}+($zoneconfigs{'sensor'}{$id}{'set'}*.2);
			my $lightsetlow=$zoneconfigs{'sensor'}{$id}{'set'}-($zoneconfigs{'sensor'}{$id}{'set'}*.2);
			print "Timer light set high $lightsethigh set low $lightsetlow  read $light \n" if $debug;
			if (  $light < $lightsetlow or $light > $lightsethigh ){
                        	if ( $light < $lightsetlow ){
					$class="isdark";
					if (-f $checklight){
						unlink $checklight;
					}
					if(! defined($isdark)){
						$isdark=day_time();
						undef $islight;
					}
                        	}
                        	if ( $light  > $lightsethigh ){
					$class="islight";
					if(! -f $checklight){
					open LIGHT, "> $checklight";
					print LIGHT day_time();
					close LIGHT;
                        		}
					if(! defined($islight)){
						$islight=day_time();
						undef $isdark;
					}
                	}
	}
			$text.="<tr><td>".$zoneconfigs{'sensor'}{$id}{'desc'}."</td><td class=\"".$class."\">".sprintf("%.1f",$zoneconfigs{'sensor'}{$id}{'value'})."</td><td>".$zoneconfigs{'sensor'}{$id}{'set'}."</td><tr>\n";
			rrd('sensor',$id);
		}else{
			save_error(get_time()." Cannot open $zoneconfigs{'sensor'}{$id}{'description'} $zoneconfigs{'sensor'}{$id}{'path'}\n");
		}
		print ">Reading $sensortype $zoneconfigs{'sensor'}{$id}{'value'}\n" if $debug;
	}
	elsif ($sensortype eq 1){
		my $valtmp=readval('sensor',$id);
		if ($valtmp < 3){
			$zoneconfigs{'sensor'}{$id}{'value'}=100;
		}else{
			$zoneconfigs{'sensor'}{$id}{'value'}=0;
		}
#		my $V100;
#		my $valtmp=readval($sensor_type);
#		$valtmp=trim($valtmp-0.05);
#		if ( $valtmp >0 && $zoneconfigs{'vdd'}{'value'}>0){
#			$V100=$waterstoragefull * $zoneconfigs{'vdd'}{'value'};
#			$zoneconfigs{'sensor'}{$id}{'value'}=sprintf("%.0f", (($valtmp/$V100) * 100) );
#		}
#		if ( $zoneconfigs{'sensor'}{$id}{'value'} < 0){
#			$zoneconfigs{'sensor'}{$id}{'value'}=0;
#		}
#		if ( $zoneconfigs{'sensor'}{$id}{'value'}> 100){
#			$zoneconfigs{'sensor'}{$id}{'value'}=100;
#		}
#		rrd($sensor_type);
		$nagios.=$zoneconfigs{'sensor'}{$id}{'desc'}.';'.$zoneconfigs{'sensor'}{$id}{'value'}.";\n";
		print ">Reading $sensortype $zoneconfigs{'sensor'}{$id}{'value'}\n" if $debug;
		$text.="<tr><td>".$zoneconfigs{'sensor'}{$id}{'desc'}."</td><td class=\"".$class."\">".sprintf("%.1f",$zoneconfigs{'sensor'}{$id}{'value'})."</td><td>".$zoneconfigs{'sensor'}{$id}{'set'}."</td><tr>\n";
	}elsif ($sensortype eq 5){
		my $valtmp=readval('sensor',$id);
		if ($valtmp){
                       	$valtmp=trim($valtmp);
		}
		if ( $valtmp < 0  || $valtmp> 100){
			if($zoneconfigs{'sensor'}{$id}{'prevalue'} >0){
				$zoneconfigs{'sensor'}{$id}{'value'}=$zoneconfigs{'sensor'}{$id}{'prevalue'};
			}
			#		}else{
			#my $diff=$zoneconfigs{'sensor'}{$id}{'prevalue'}-$valtmp;
			#if($diff >10 || ($diff <-10 && $zoneconfigs{'sensor'}{$id}{'prevalue'} >0)){
			#	$zoneconfigs{'sensor'}{$id}{'value'}=$zoneconfigs{'sensor'}{$id}{'prevalue'};
			#}else{
			#	$zoneconfigs{'sensor'}{$id}{'value'}=$valtmp;
			#	$zoneconfigs{'sensor'}{$id}{'prevalue'}=$valtmp;
			#}
		}else{
			$zoneconfigs{'sensor'}{$id}{'value'}=$valtmp;
			$zoneconfigs{'sensor'}{$id}{'prevalue'}=$valtmp;
		}
		my $hum=$zoneconfigs{'sensor'}{$id}{'value'};
		my $humsethigh=$zoneconfigs{'sensor'}{$id}{'set'}+5;
		my $humsetlow=$zoneconfigs{'sensor'}{$id}{'set'}-5;
		print "Timer hum set high $humsethigh set low $humsetlow  read $hum \n" if $debug;
		if (  $hum < $humsetlow or $hum > $humsethigh ){
                       	if ( $hum < $humsetlow ){
				$class="isdry";
                       	}
                       	if ( $hum  > $humsethigh ){
				$class="iswet";
			}
               	}
		rrd('sensor',$id);
		$nagios.=$zoneconfigs{'sensor'}{$id}{'desc'}.';'.$zoneconfigs{'sensor'}{$id}{'value'}.";\n";
		print "Reading $sensortype $zoneconfigs{'sensor'}{$id}{'value'}\n" if $debug;
		$text.="<tr><td>".$zoneconfigs{'sensor'}{$id}{'desc'}."</td><td class=\"".$class."\">".sprintf("%.0f",$zoneconfigs{'sensor'}{$id}{'value'})."</td><td>".$zoneconfigs{'sensor'}{$id}{'set'}."</td><tr>\n";
		}
	}

sub avg
  {
    return undef if !scalar(@_);
    my $c = 0;
    my $sum="0";
    my $avg;
    foreach (@_) {
        $sum += $_;
        print "$_ " if $debug;
        $c++;
        }
        if ($sum > 0){
                $avg=$sum / $c;
                $avg=sprintf("%.1f", $avg);
        } else {
                $avg="0";
        }
    return $avg;
  }

sub save_error {
    return undef if !scalar(@_);
	open  ERROR, ">>$errorfile";
	print ERROR "$_";
	close ERROR;
}
sub temp_controlsec(){
	my $id=shift;
	my $temp_set;
	my $temp_set_upper;
	my $temp_set_low;
	my $class;
	if($zoneconfigs{'zone'}{$id}{'iosec'} && $zoneconfigs{'zone'}{$id}{'sethigh'}){
		$temp_set=$zoneconfigs{'zone'}{$id}{'sethigh'};
        	if($zoneconfigs{'zone'}{$id}{'hyst'}){
			$temp_set_upper=$temp_set + ($zoneconfigs{'zone'}{$id}{'hyst'}/2);
			$temp_set_low=$temp_set - ($zoneconfigs{'zone'}{$id}{'hyst'}/2);
		}else{
			$temp_set_upper=$temp_set + $hyst;
			$temp_set_low=$temp_set - $hyst;
		}
		print "Temp set sec $temp_set ($temp_set_upper $temp_set_low) $zoneconfigs{'zone'}{$id}{'hyst'}\n" if $debug;
		if (  $zoneconfigs{'zone'}{$id}{'value'} < $temp_set_low or $zoneconfigs{'zone'}{$id}{'value'} > $temp_set_upper ){
       			if ( $zoneconfigs{'zone'}{$id}{'value'} < $temp_set_low ){
				$zoneconfigs{'zone'}{$id}{'statushigh'}=$on;
        		}
        		if ( $zoneconfigs{'zone'}{$id}{'value'} > $temp_set_upper ){
				$zoneconfigs{'zone'}{$id}{'statushigh'}=$off;
        		}
		}
		if($zoneconfigs{'zone'}{$id}{'statushigh'}==$on){
			if($zoneconfigs{'zone'}{$id}{'levelsec'} eq "0"){
				iosec($id,$on);
				$class="control_on";
				$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'zone'}{$id}{'desc'}." sec</div>\n";
			}else{
				iosec($id,$off);
			}
		}else{
			if($zoneconfigs{'zone'}{$id}{'levelsec'} eq "0"){
				iosec($id,$off);
			}else{
				$class="control_on";
				$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'zone'}{$id}{'desc'}." sec</div>\n";
				iosec($id,$on);
			}
		}
	print "Control $zoneconfigs{'zone'}{$id}{'status'} sec level $zoneconfigs{'zone'}{$id}{'levelsec'}\n" if $debug;
	}
}
sub check_timer_heater{
        my $id=shift;
        my $connection = connectmysqldb();
        my $query = "SELECT starttime,endtime,settemp FROM program  WHERE zid = '$id';";
         my $statement = $connection->prepare($query);
        $statement->execute();
        while (my @data = $statement->fetchrow_array()) {
               my $period_on_sec=$data[0];
               my $period_off_sec=$data[1];
                my $day_sec=day_time();
                if ($period_on_sec >$period_off_sec){
#                               print "$day_sec > $period_on_sec and $day_sec < $day-1\n" if $debug;
                        if ($day_sec > $period_on_sec and $day_sec < $day-1){
                                $period_off_sec=$day-1;
                        }else{
                                $period_on_sec="0";
                        }
                }
#                        print "$day_sec > $period_on_sec and $day_sec < $period_off_sec set $data[2]\n" if $debug;
               if ( $day_sec > $period_on_sec and $day_sec < $period_off_sec ){
                        return($data[2]);
                }

        }
#       print "Timer= $timer\n" if $debug;
}


sub temp_control(){
	my $id=shift;
	my $temp_set;
	my $temp_set_upper;
	my $temp_set_low;
	my $class;
       	if ($zoneconfigs{'zone'}{$id}{'oper'}  eq "2"){
		$temp_set=check_timer_heater($id);
        	if (!$temp_set){
			$temp_set=$zoneconfigs{'zone'}{$id}{'set'}
		}
		$zoneconfigs{'zone'}{$id}{'setrrd'}=$temp_set;
        	if($zoneconfigs{'zone'}{$id}{'hyst'}){
			$temp_set_upper=$temp_set + ($zoneconfigs{'zone'}{$id}{'hyst'}/2);
			$temp_set_low=$temp_set - ($zoneconfigs{'zone'}{$id}{'hyst'}/2);
		}else{
			$temp_set_upper=$temp_set + $hyst;
			$temp_set_low=$temp_set - $hyst;
		}
		print "Temp set $temp_set ($temp_set_upper $temp_set_low) $zoneconfigs{'zone'}{$id}{'hyst'}\n" if $debug;
		if (  $zoneconfigs{'zone'}{$id}{'value'} < $temp_set_low or $zoneconfigs{'zone'}{$id}{'value'} > $temp_set_upper ){
       			if ( $zoneconfigs{'zone'}{$id}{'value'} < $temp_set_low ){
				$zoneconfigs{'zone'}{$id}{'status'}=$on;
        		}
        		if ( $zoneconfigs{'zone'}{$id}{'value'} > $temp_set_upper ){
				$zoneconfigs{'zone'}{$id}{'status'}=$off;
        		}
		}
       	}elsif($zoneconfigs{'zone'}{$id}{'oper'}  eq "1"){
			$zoneconfigs{'zone'}{$id}{'status'}=$on;
	}else{
			$zoneconfigs{'zone'}{$id}{'status'}=$off;
	}
	if($zoneconfigs{'zone'}{$id}{'status'}==$on){
		$zonestatus{$zoneconfigs{'zone'}{$id}{'address'}}{$zoneconfigs{'zone'}{$id}{'io'}}=$on;
		$class="control_on";
		$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'zone'}{$id}{'desc'}."</div>\n";
#		if(!$zoneconfigs{'zone'}{$id}{'starttime'}){
#			$zoneconfigs{'zone'}{$id}{'starttime'}=time();
#			$zoneconfigs{'zone'}{$id}{'timerid'}=zone_starttimerdb($zoneconfigs{'zone'}{$id}{'id'},$zoneconfigs{'zone'}{$id}{'starttime'});
#		}
		if($zoneconfigs{'zone'}{$id}{'io'}){
#			ioprim($zoneconfig,$on);
		}
	}else{
		$class="control_off";
		if($zoneconfigs{'zone'}{$id}{'io'}){
			if($zonestatus{$zoneconfigs{'zone'}{$id}{'address'}}{$zoneconfigs{'zone'}{$id}{'io'}} ne $on){
				$zonestatus{$zoneconfigs{'zone'}{$id}{'address'}}{$zoneconfigs{'zone'}{$id}{'io'}}=$off;
#				ioprim($zoneconfig,$off);
			}
		}
		if($zoneconfigs{'zone'}{$id}{'starttime'}){
			my $timer=time()-$zoneconfigs{'zone'}{$id}{'starttime'};
			zone_endtimerdb($zoneconfigs{'zone'}{$id}{'timerid'},$timer);
			undef $zoneconfigs{'zone'}{$id}{'starttime'};
			undef $zoneconfigs{'zone'}{$id}{'timerid'};
		}
	}
	$text.="<tr><td>".$zoneconfigs{'zone'}{$id}{'desc'}."</td><td class=\"".$class."\">".sprintf("%.1f",$zoneconfigs{'zone'}{$id}{'value'})."</td><td>".$temp_set."</td><tr>\n";
}

sub check_timer{
	my $id=shift;
        my $period_on_sec=$zoneconfigs{'timer'}{$id}{'start'};
        my $period_off_sec=$zoneconfigs{'timer'}{$id}{'end'};
        my $day_sec=day_time();
		if ($period_on_sec >$period_off_sec){
#			print "$day_sec > $period_on_sec and $day_sec < $day-1\n" if $debug;
			if ($day_sec > $period_on_sec and $day_sec < $day-1){
				$period_off_sec=$day-1;
			}else{
				$period_on_sec="0";
			}
		}
#		print " uit\n" if $debug;
#                       print "$period_on_sec > $day_sec < $period_off_sec\n" if $debug;
                if ( $day_sec > $period_on_sec and $day_sec < $period_off_sec ){
                           return("on");
		}
}
sub timer_control(){
	my $id=shift;
	my $class;
#		'0' => '',
#                '1' => t('waterstorage'),
#                '2' => t('light'),
#                '3' => t('sunrise/sunset'),
#                '4' => t('temperature'),
#                '5' => t('humidity'));
	my $sensortype=$zoneconfigs{'sensor'}{$zoneconfigs{'timer'}{$id}{'sensorid'}}{'sensortype'};
	my $sensorvalue=$zoneconfigs{'sensor'}{$zoneconfigs{'timer'}{$id}{'sensorid'}}{'value'};
	if ($zoneconfigs{'timer'}{$id}{'oper'} eq "2"){
		if ((check_timer($id)) eq "on"){
			if($sensortype eq '1') {
                                if ($sensorvalue eq $zoneconfigs{'timer'}{$id}{'sensorvalue'}){
					$zoneconfigs{'timer'}{$id}{'status'}=$on;
                        	}else{
					$zoneconfigs{'timer'}{$id}{'status'}=$off;
				}
			}elsif($sensortype eq '2') {
			        my $light=$sensorvalue;
				my $lightsethigh=$zoneconfigs{'timer'}{$id}{'sensorvalue'}+($zoneconfigs{'timer'}{$id}{'sensorvalue'}*.1);
				my $lightsetlow=$zoneconfigs{'timer'}{$id}{'sensorvalue'}-($zoneconfigs{'timer'}{$id}{'sensorvalue'}*.1);
				print "Timer light set high $lightsethigh set low $lightsetlow  read $light \n" if $debug;
				if (  $light < $lightsetlow or $light > $lightsethigh ){
                        		if ( $light < $lightsetlow ){
						$zoneconfigs{'timer'}{$id}{'status'}=$on;
						print "Timer $id aan te weinig licht\n" if $debug;
                        		}
                        		if ( $light  > $lightsethigh ){
						$zoneconfigs{'timer'}{$id}{'status'}=$off;
						print "Timer $id aan licht genoeg\n" if $debug;
                        		}
                		}

			}elsif($sensortype eq '3') {
				my $adjust=$zoneconfigs{'timer'}{$id}{'sensorvalue'};
				if (sunrise_set($adjust)){
					if(defined($isdark)){
						$zoneconfigs{'timer'}{$id}{'status'}=$on;
	#					print "Timer $id aan \n" if $debug;
					}
				}else{
					if(defined($islight)){
						$zoneconfigs{'timer'}{$id}{'status'}=$off;
					}
				}
			}elsif($sensortype eq '4') {
				if ( $sensorvalue > $zoneconfigs{'timer'}{$id}{'sensorvalue'}){
                                        $zoneconfigs{'timer'}{$id}{'status'}=$on;
#                                       print "Timer $id aan \n" if $debug;
                                }else{
                                        $zoneconfigs{'timer'}{$id}{'status'}=$off;
                                }
			}elsif($sensortype eq '5') {
				print "timer humidity $zoneconfigs{'humidity'}{'value'} < $zoneconfigs{'timer'}{$id}{'sensorvalue'}\n" if $debug;
                                if ($sensorvalue < $zoneconfigs{'timer'}{$id}{'sensorvalue'}){
                                        $zoneconfigs{'timer'}{$id}{'status'}=$on;
#                                       print "Timer $id aan \n" if $debug;
                                }else{
                                        $zoneconfigs{'timer'}{$id}{'status'}=$off;
                                }
			}else{
				$zoneconfigs{'timer'}{$id}{'status'}=$on;
			}
		}else{
			$zoneconfigs{'timer'}{$id}{'status'}=$off;
#			print "Timer $id uit\n" if $debug;
		}
	}elsif ($zoneconfigs{'timer'}{$id}{'oper'} eq "3"){
		$zoneconfigs{'timer'}{$id}{'status'}=$off;
	}elsif ($zoneconfigs{'timer'}{$id}{'oper'} eq "1"){
		$zoneconfigs{'timer'}{$id}{'status'}=$on;
	}elsif($zoneconfigs{'timer'}{$id}{'oper'} eq "0"){
		$zoneconfigs{'timer'}{$id}{'status'}=$off;
	}

	if($zoneconfigs{'timer'}{$id}{'status'}==$on){
		if ($zoneconfigs{'timer'}{$id}{'duration'} >0 && $zoneconfigs{'timer'}{$id}{'duration'} < $loop){
                        print "Timer aan voor $id $zoneconfigs{'timer'}{$id}{'duration'} \n" if $debug;
                        ioprim($id,$on);
                        sleep($zoneconfigs{'timer'}{$id}{'duration'});
                        ioprim($id,$off);
                        $zoneconfigs{'timer'}{$id}{'status'}=$off;
                        print "Timer uit voor $id $zoneconfigs{'timer'}{$id}{'duration'} \n" if $debug;
                        $timerstatus{$zoneconfigs{'timer'}{$id}{'address'}}{$zoneconfigs{'timer'}{$id}{'io'}}=$off;
                }else{
			if($zoneconfigs{'timer'}{$id}{'io'}){
				if($timerstatus{$zoneconfigs{'timer'}{$id}{'address'}}{$zoneconfigs{'timer'}{$id}{'io'}} ne $on){
#					$log.=get_time(). " timer ".$zoneconfigs{'timer'}{$id}{'desc'}.": on\n";
				}
				$timerstatus{$zoneconfigs{'timer'}{$id}{'address'}}{$zoneconfigs{'timer'}{$id}{'io'}}=$on;
				$class="control_on";
			}else{
				$class="control_off";
			}
			$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'timer'}{$id}{'desc'}."</div>\n";
		}
	}else{
		$class="control_off";
		if($zoneconfigs{'timer'}{$id}{'io'}){
			if($timerstatus{$zoneconfigs{'timer'}{$id}{'address'}}{$zoneconfigs{'timer'}{$id}{'io'}} ne $on){
				$timerstatus{$zoneconfigs{'timer'}{$id}{'address'}}{$zoneconfigs{'timer'}{$id}{'io'}}=$off;
			}
		}
	}
}
sub check_feeder{
        my $id=shift;
        my $period_on_sec=$zoneconfigs{'timer'}{$id}{'start'};
        my $day_sec=day_time();
                if ( $day_sec > $period_on_sec and $day_sec < $period_on_sec+$loop ){
                           return("on");
                }
}
sub feeder_control(){
        my $id=shift;
        my $class;
#                               $zoneconfigs{@data[1]}{'address'}=$address;
#                                $zoneconfigs{@data[1]}{'id'}=$data[0];
#                                $zoneconfigs{@data[1]}{'start'}=$data[2];
#                                $zoneconfigs{@data[1]}{'oper'}=@data[11];
#                                $zoneconfigs{@data[1]}{'desc'}=@data[12];

        if ($zoneconfigs{'timer'}{$id}{'oper'} eq "2"){
                if ((check_feeder($id)) eq "on"){
			print "Feeder aan\n" if $debug;
                        ioprim($id,$on);
                        sleep(2);
                        ioprim($id,$off);
#			$log.=get_time(). " feeder ".$zoneconfigs{'timer'}{$id}{'desc'}.": on\n";
                }else{
                	ioprim($id,$off);
		}
        }elsif ($zoneconfigs{'timer'}{$id}{'oper'} eq "1"){
                $zoneconfigs{'timer'}{$id}{'status'}=$on;
		print "Feeder aan\n" if $debug;
                ioprim($id,$on);
                sleep(5);
		print "Feeder uit\n" if $debug;
                ioprim($id,$off);
        }elsif($zoneconfigs{'timer'}{$id}{'oper'} eq "0"){
                $zoneconfigs{'timer'}{$id}{'status'}=$off;
        }

}

sub vent_control(){
	my $id=shift;
	my $temp_set_upper;
	my $temp_set_low;
	my $class;
       	if ($zoneconfigs{'ventilation'}{$id}{'oper'}  eq "2"){
		my $windspeed_set_upper=$zoneconfigs{'ventilation'}{$id}{'wind'}  + 2;
		my $windspeed_set_low=$zoneconfigs{'ventilation'}{$id}{'wind'} - 2;
		my $windspeed=@weer[10];
		my $temp_set_upper=$zoneconfigs{'ventilation'}{$id}{'set'}  + $zoneconfigs{'ventilation'}{$id}{'hyst'};
		my $temp_set_low=$zoneconfigs{'ventilation'}{$id}{'set'} - $zoneconfigs{'ventilation'}{$id}{'hyst'};
		my $temperature=$zoneconfigs{$zoneconfigs{'ventilation'}{$id}{'zone'}}{'value'};
		print "$zoneconfigs{'ventilation'}{$id}{'desc'} temp_set_upper $temp_set_upper temp_set_low $temp_set_low temperature $temperature sleep $zoneconfigs{'ventilation'}{$id}{'sleep'} sleep2 $zoneconfigs{'ventilation'}{$id}{'sleep2'} rise $zoneconfigs{'ventilation'}{$id}{'rise'} wind $windspeed huidig $zoneconfigs{'ventilation'}{$id}{'wind'}\n" if $debug;
		#Ventilatie dicht met meer dan maxwind ms
		if (  $windspeed < $windspeed_set_low or $windspeed > $windspeed_set_upper ){
        		if ( $windspeed  > $windspeed_set_upper ){
				if($zoneconfigs{'ventilation'}{$id}{'io'}){
					undef $zoneconfigs{'ventilation'}{$id}{'status1'} ;
					undef $zoneconfigs{'ventilation'}{$id}{'status2'} ;
					iovent($id,$off,$maxwind,1);
					$class="control_warning";
					$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'ventilation'}{$id}{'desc'}." ramen dicht ivm harde wind</div>\n";
					print "$zoneconfigs{'ventilation'}{$id}{'desc'} closed\n" if $debug;
				}
			}
        		if ( $windspeed  < $windspeed_set_low ){
			   print "Waait niet te hard: $windspeed  < $windspeed_set_low\n" if $debug;
				if (  $temperature < $temp_set_low or $temperature > $temp_set_upper ){
					print "Lager of hoger dan $temperature < $temp_set_low or $temperature > $temp_set_upper\n" if $debug;
       					if ( $temperature < $temp_set_low ){
						if($zoneconfigs{'ventilation'}{$id}{'io'}){
							iovent($id,$off,$zoneconfigs{'ventilation'}{$id}{'sleep'},1);
							print "$zoneconfigs{'ventilation'}{$id}{'desc'} closed $temperature < $temp_set_low \n" if $debug;
						}
        				}
        				if ( $temperature  > $temp_set_upper ){
						if($zoneconfigs{'ventilation'}{$id}{'io'}){
							iovent($id,$on,$zoneconfigs{'ventilation'}{$id}{'sleep'},1);
							print "$zoneconfigs{'ventilation'}{$id}{'desc'} open $temperature  > $temp_set_upper  \n" if $debug;
							$class="control_on";
						}
        				}
				}
				if (  $temperature < ($temp_set_low+$zoneconfigs{'ventilation'}{$id}{'rise'}) or $temperature > ($temp_set_upper+$zoneconfigs{'ventilation'}{$id}{'rise'}) ){
        				if ( $temperature  > ($temp_set_upper+$zoneconfigs{'ventilation'}{$id}{'rise'}) ){
						if($zoneconfigs{'ventilation'}{$id}{'io'}){
							iovent($id,$on,$zoneconfigs{'ventilation'}{$id}{'sleep2'},2);
							print "$zoneconfigs{'ventilation'}{$id}{'desc'} open2 $temp_set_upper+$zoneconfigs{'ventilation'}{$id}{'rise'}\n" if $debug;
							$class="control_on";
						}
        				}
       					if ( $temperature < ($temp_set_low+$zoneconfigs{'ventilation'}{$id}{'rise'}) && $temperature > $zoneconfigs{'ventilation'}{$id}{'set'} ){
						if($zoneconfigs{'ventilation'}{$id}{'io'}){
							iovent($id,$off,$zoneconfigs{'ventilation'}{$id}{'sleep2'}-1,2);
							print "$zoneconfigs{'ventilation'}{$id}{'desc'} closed2\n" if $debug;
						}
        				}
				}
			}
		}else{
			print "$windspeed < $windspeed_set_low or $windspeed > $windspeed_set_upper \n" if $debug;
		}
	}elsif($zoneconfigs{'ventilation'}{$id}{'oper'}  eq "1"){
		$class="control_on";
		iovent($id,$on,2,1);
		print "$zoneconfigs{'ventilation'}{$id}{'desc'} open\n" if $debug;
	}else{
		$class="control_off";
#		iovent($id,$off,2,1);
		print "$zoneconfigs{'ventilation'}{$id}{'desc'} closed\n" if $debug;
	}
	my $ventstatus=0;
        for (my $step=1; $step <= 2; $step++) {
		if( $zoneconfigs{'ventilation'}{$id}{'status'.$step} eq 1){
			$ventstatus=1;
		}
		if( $zoneconfigs{'ventilation'}{$id}{'status'.$step} eq 2){
			$ventstatus=2;
			$class="control_vent";
			$status.="<div class=\"ctlfloatleft ".$class."\">".$zoneconfigs{'ventilation'}{$id}{'desc'}." stand ".$step."</div>\n";
		}
	}
#	$nagios.=zoneconfigs{$zoneconfig}{'desc'}.';'.$ventstatus."\n";
}


sub get_time(){
	return(POSIX::strftime("%Y-%m-%d %H:%M:%S", localtime));
}

sub day_time(){
	my($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
        return($sec + ($min * 60) + ($hour * 3600));
}

sub print_result(){
	#tabel
	$text=$text . "<tr><td colspan=3><center>" . get_time() . "</center></td></tr>\n";
	$text=$text . "</table>\n";
	open OUT, ">$out" || print "Cannot open $out"; 
	printf OUT $text;
	close OUT;
	#status regel
	open OUT, ">$statusout" || print LOG "Cannot open $statusout";
        printf OUT $status;
	close OUT;
	#nagios
	open OUT, ">$nagioscheck" || print LOG "Cannot open $nagioscheck";
        printf OUT $nagios;
	close OUT;
	open OUT, ">>$logfile" || print LOG "Cannot open $logfile";
        printf OUT $log;
	close OUT;
	undef $log;
	$log='';
}


sub readval(){
	my $zone=shift;
	my $id=shift;
	print "Read sensor $zone $zoneconfigs{$zone}{$id}{'address'},$zoneconfigs{$zone}{$id}{'path'}\n" if $debug;
	my $val=OWNet::read($zoneconfigs{$zone}{$id}{'address'},$zoneconfigs{$zone}{$id}{'path'}) or errordev($zoneconfigs{$zone}{$id}{'address'}.','.$zoneconfigs{$zone}{$id}{'path'}.' '.$zoneconfigs{$zone}{$id}{'desc'},$!);
	return($val);
}

sub writeio{
	my $address=shift;
	my $device=shift;
	my $val=shift;
	owwrite("$device",$val);	
}

sub ioprim(){
	my $id=shift;
	my $val=shift;
	print "OWnet write $id $zoneconfigs{'timer'}{$id}{'address'},$zoneconfigs{'timer'}{$id}{'io'},$val\n" if $debug;
	if($zoneconfigs{'timer'}{$id}{'io'}){
#		OWNet::write($zoneconfigs{'timer'}{$id}{'address'},$zoneconfigs{'timer'}{$id}{'io'},$val);
		owwrite("$zoneconfigs{'timer'}{$id}{'io'}",$val);	
	}
}
sub iosec(){
	my $id=shift;
	my $val=shift;
	print "OWnet write $id $zoneconfigs{'timer'}{$id}{'address'},$val\n" if $debug;
	if($zoneconfigs{'timer'}{$id}{'iosec'}){
		owwrite("$zoneconfigs{'timer'}{$id}{'iosec'}",$val);	
	}
}
sub iovent(){
	my $id=shift;
	my $val=shift;
	my $sleep=shift;
	my $step=shift;
	if($zoneconfigs{'timer'}{$id}{'io'}){
		if($val eq $on && $zoneconfigs{'timer'}{$id}{'status'.$step} ne 2){
			owwrite("$zoneconfigs{'timer'}{$id}{'io'}",$on);	
			sleep($sleep);
			owwrite("$zoneconfigs{'timer'}{$id}{'io'}",$off);	
			$zoneconfigs{'timer'}{$id}{'status'.$step}='2';	
		}elsif($val eq $off && $zoneconfigs{'timer'}{$id}{'status'.$step} ne 1 ){
			print "$zoneconfigs{'timer'}{$id}{'desc'} close\n" if $debug;
			owwrite("$zoneconfigs{'timer'}{$id}{'io2'}",$on);	
			sleep($sleep);
			owwrite("$zoneconfigs{'timer'}{$id}{'io2'}",$off);	
			$zoneconfigs{'timer'}{$id}{'status'.$step}='1';	
		}
	}
}
sub owwrite(){
	my $device=shift;
	my $val=shift;
	my $dev=0;
	my $address;
	my @owaddresses=(split(/\s+/,$owaddress));
	for $address (@owaddresses){
		if($dev eq 1){
			last;
		}
		print "OWnet read IO $address,$device,$val\n" if $debug;
#               	if(OWNet::read("$address","$device")){
               	if(OWNet::present("$address","$device")){
			print "OWnet write IO $address,$device,$val\n" if $debug;
			OWNet::write("$address","$device",$val) or errordev($address.','.$device,$!);
			$dev=1;
		}
	}
	if($dev eq 0){
		$deviceserror.=@owaddresses.' '.$device.' val:'.$val." not found\n" if $debug;
	 	print $address. ' '.$device." not found\n" if $debug;
	}

}

sub check_hosts(){
        my $addresses=shift;
        my $address;
	my $ow;
        my @owaddresses=(split(/\s+/,$addresses));
        for $address (@owaddresses){
                (my $host,my $port)=(split(/:/,$address));
                my $p = Net::Ping->new("tcp",5);
                $p->port_number(getservbyname($port, "tcp"));
                if ($p->ping($host)){
                        $ow.=$address. " ";
                }else{
			print "timeout $host $port\n"
		}
                $p->close();
        }
	print "Hosts found $ow\n" if $debug;
        return $ow;
}

sub errordev(){
        my $dev=shift;
        my $error=shift;
        $deviceserror.=$dev.' '.$error." error reading or writing\n";
        print $dev.' '.$error." error reading or writing " if $debug;
}
sub lightfilestatus(){
	if(sunrise_pre()){
		if (-f $checklight){
			unlink $checklight;
		}
	}else{
		if(! -f $checklight){
		open LIGHT, "> $checklight";
		print LIGHT "light";
		close LIGHT;
		}
	}
}
sub sunrise_set(){
#        get_time();
	my $adjust=shift;
	if( ! defined($adjust)){
		$adjust=1800;
	}
	my $sunrise;
	my $sunset;
	my $sunriseh;
	my $sunrisem;
	my $sunseth;
	my $sunsetm;
	my $dst;
	my $value;
	my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
	my $day_sec=($hour*3600)+($min*60)+$sec;
	my $sunrise=sun_rise($longitude,$latitude);
        my $sunset=sun_set($longitude,$latitude);
        ($sunriseh,$sunrisem)=split(/:/,$sunrise);
	#my $sunrisesec=($sunrisem * 60) + ($sunriseh * 3600)+$adjust;
	my $sunrisesec=($sunrisem * 60) + ($sunriseh * 3600);
        ($sunseth,$sunsetm)=split(/:/,$sunset);
         my $sunsetsec=(($sunsetm * 60) + ($sunseth * 3600))-$adjust;
         my $period_sec=($min * 60) + ($hour * 3600);
#        print "Sunrise: $sunriseh:$sunrisem, Sunset $sunseth:$sunsetm $day_sec > $sunrisesec and $day_sec < $sunsetsec\n" if $debug;
        if ($day_sec > $sunrisesec and $day_sec < $sunsetsec){
              undef $value;
#              print "Het is licht\n" if $debug;
        }else{
                $value=1;
#                print "Het is donker\n" if $debug;
        }
        return($value);
}
sub sunrise_pre(){
#        get_time();
	my $sunrise;
	my $sunset;
	my $sunriseh;
	my $sunrisem;
	my $sunseth;
	my $sunsetm;
	my $dst;
	my $value;
	my ($sec,$min,$hour,$mday,$mon,$year) = localtime(time);
	my $day_sec=($hour*3600)+($min*60)+$sec;
	my $sunrise=sun_rise($longitude,$latitude);
        my $sunset=sun_set($longitude,$latitude);
        ($sunriseh,$sunrisem)=split(/:/,$sunrise);
         my $sunrisesec=($sunrisem * 60) + ($sunriseh * 3600)-1800;
        ($sunseth,$sunsetm)=split(/:/,$sunset);
         my $sunsetsec=(($sunsetm * 60) + ($sunseth * 3600))+3600;
         my $period_sec=($min * 60) + ($hour * 3600);
#        print "Sunrise: $sunriseh:$sunrisem, Sunset $sunseth:$sunsetm $day_sec > $sunrisesec and $day_sec < $sunsetsec\n" if $debug;
        if ($day_sec > $sunrisesec and $day_sec < $sunsetsec){
              undef $value;
#              print "Het is licht\n" if $debug;
        }else{
                $value=1;
#                print "Het is donker\n" if $debug;
        }
        return($value);
}

sub touch(){
	my $lock=shift;
#	print "Creating Lock file: $lock\n" if $debug;
	open FILE, ">$lock" || print "Cannot open $lock";
	print FILE ".";
	close FILE;
}
sub checkDaylightSavingsTime {

  # Check to see if the daylight savings time is currently in effect.
  # It starts the first Sunday in April and ends the last Sunday in October.

  # Initialize variables
  my ($daylightSavingsTime,$apr,$oct) = ("no",3,9);

  # Get the current time
  my ($day,$month,$weekday) = (localtime)[3,4,6];

  if ($month > $apr && $month < $oct) {
     $daylightSavingsTime = "yes";
  } elsif ($month == $apr) {
     $daylightSavingsTime = "yes" if ($day - $weekday) >= 1;
  } elsif ($month == $oct) {
     my $daysUntilSunday = (7 - $weekday);
     $daylightSavingsTime = "yes" if ($day + $daysUntilSunday) <= 31;
  } # end if

  return ($daylightSavingsTime);

} # end sub



sub rrd(){
	my $zone=shift @_;
	my $id= shift @_;
	my $sensortype;
	#Get all rrd db's
	my $basename=$zone.'_'.$zoneconfigs{$zone}{$id}{'id'}.'_'.$zoneconfigs{$zone}{$id}{'name'};
	if( $zone eq 'sensor'){
                $sensortype=$zoneconfigs{'sensor'}{$id}{'sensortype'};
        }
        my $desc=$zoneconfigs{$zone}{$id}{'desc'};
        if(!$desc){
                my $desc=$zoneconfigs{$zone}{$id}{'name'};
        }
	
	my $socket;
	if($socket = IO::Socket::INET->new(PeerAddr=> $rrdhost, PeerPort=> $rrddbport, Proto=> 'tcp', Timeout => 3, Type=> SOCK_STREAM)){
        	my $line;
        	my $dbs;
        	while ($line = <$socket>) {
                	$dbs.=$line;
        	}
        	close $socket;
#	print "RRDs: $dbs" if $debug;
	
		my $time_string=get_time();
		if($socket = IO::Socket::INET->new(PeerAddr=> $rrdhost, PeerPort=> $rrdport, Proto=> 'tcp', Timeout => 3, Type=> SOCK_STREAM)){
			if ( $dbs=~ /$basename/ ){
				print "bestaat: $db/${basename}.rrd\n" if $debug;
       				print $socket "update $db/${basename}.rrd N:$zoneconfigs{$zone}{$id}{'value'}\n";
       				print "RRD update $db/${basename}.rrd N:$zoneconfigs{$zone}{$id}{'value'}\n" if $debug;
			}else{
				print "create $db/${basename}.rrd\n" if $debug;
		 		print $socket "create $db/${basename}.rrd --step 60 --start N DS:temp:GAUGE:300:U:U RRA:AVERAGE:0.5:1:525600 RRA:MIN:0.5:12:525600 RRA:MAX:0.5:12:525600 RRA:LAST:0.5:12:525600\n";
			}
        		close $socket;
		}
		my $width=300;
		my $height=80;
		#               '0'  => '',
#                '1' => t('waterstorage'),
#                '2' => t('light'),
#                '3' => t('sunrise/sunset'),
#                '4' => t('temperature'),
#                '5' => t('humidity'),
#                '9' => t('VDD'),
		if($socket = IO::Socket::INET->new(PeerAddr=> $rrdhost, PeerPort=> $rrdport, Proto=> 'tcp', Timeout => 3, Type=> SOCK_STREAM)){
			if ($sensortype eq "5"){
				print "RRD graph $basename humidity" if $debug;
				print "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -u 100 -l 0 -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"lv %\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} ' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n" if $debug; 
				print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -u 100 -l 0 -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"lv %\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} ' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
			}elsif ($sensortype eq "2"){
				print "RRD graph $basename licht" if $debug;
				print "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -o --units=si  -r -u 100000 -l 100  -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"lux\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ffae00:$zoneconfigs{$zone}{$id}{'desc'} ' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n" if $debug;
				print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -o --units=si  -r -u 100000 -l 100  -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"lux\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ffae00:$zoneconfigs{$zone}{$id}{'desc'} ' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
			}elsif($zone eq 'zone'){
				print "RRD graph $basename temp" if $debug;
				print "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"Graden C\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} Temp' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n" if $debug;
				print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}.png -E -a PNG -w $width -h $height -t \"$zoneconfigs{$zone}{$id}{'desc'} $time_string\"  --vertical-label \"Graden C\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:LAST' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} Temp' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
#			print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}_week.png -E -a PNG -s end-1week -w $width -h $height  -t \"$zoneconfigs{$zone}{$id}{'desc'} temperatuur $time_string\"  --vertical-label \"Graden C\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:AVERAGE' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} Temp' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
#			print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}_maand.png -E -a PNG -s end-1month -w $width -h $height  -t \"$zoneconfigs{$zone}{$id}{'desc'} temperatuur $time_string\"  --vertical-label \"Graden C\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:AVERAGE' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} Temp' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
#			print $socket "graph --color GRID#006600 --color MGRID#005000 $www/${basename}_jaar.png -E -a PNG -w 800 -h 200 -s end-12month  -t \"$zoneconfigs{$zone}{$id}{'desc'} temperatuur $time_string\"  --vertical-label \"Graden C\" 'DEF:${basename}_temp=$db/${basename}.rrd:temp:AVERAGE' 'LINE1:${basename}_temp#ff0000:$zoneconfigs{$zone}{$id}{'desc'} Temp' 'GPRINT:${basename}_temp:LAST:\\:%2.1lf'  'GPRINT:${basename}_temp:MIN:Min\\: %2.1lf' 'GPRINT:${basename}_temp:MAX:Max\\: %2.1lf'  'GPRINT:${basename}_temp:AVERAGE:Gem\\: %2.1lf C\\n'  \n";
			}
		}else{
			print "Cannot op socket PeerAddr=> $rrdhost, PeerPort=> $rrdport, Proto=> 'tcp'\n" if $debug; 
		}

	}else{
		print "Cannot op socket PeerAddr=> $rrdhost, PeerPort=> $rrddbport, Proto=> 'tcp'\n" if $debug; 
	}
}


sub owfs_temp_init(){
	my @zones;
	my @data;
# set the value of your SQL query
#0id      1zone    2device  3file    4settemp 5settemphigh     6hystersis       7program 8iodevice        9iodevnr 10devfile 11iodevicesec 12devfilesec       13iodevsecnr      14operation       15description
	my @owaddresses=(split(/\s+/,$owaddress));
	my $connection = connectmysqldb();
	my $query = "SELECT z.id AS id, z.zone AS zone, d.device AS device, d.file AS file, z.settemp AS settemp, z.settemphigh AS settemphigh, z.hystersis AS hystersis, z.program AS program, d2.device AS iodevice, z.iodevnr AS iodevnr, d2.file AS devfile, d3.device AS iodevicesec, d2.file AS devfilesec, z.iodevsecnr AS iodevsecnr, z.operation AS operation, z.description AS description,z.iolevelsec as iolevelsec FROM zone z LEFT OUTER JOIN 1wdevices d ON z.device=d.id LEFT OUTER JOIN 1wdevices d2 ON z.iodev=d2.id LEFT OUTER JOIN 1wdevices d3 ON z.iodevsec=d3.id";
	 my $statement = $connection->prepare($query);
	$statement->execute();
   	while (@data = $statement->fetchrow_array()) {
		my $device=0;
       		for my $address (@owaddresses){
		undef my $type;
		undef my $owserver;
			my $owserver = OWNet->new($address) ;
			my $type = $owserver->read("/".@data[2]."/temperature") ;
			if ( defined($type) ) {
                        	$zoneconfigs{'zone'}{@data[0]}{'id'}=$data[0];
                        	$zoneconfigs{'zone'}{@data[0]}{'address'}=$address;
                        	$zoneconfigs{'zone'}{@data[0]}{'name'}=$data[1];
                        	$zoneconfigs{'zone'}{@data[0]}{'path'}="/".@data[2]."/".@data[3];
				if(@data[8]){
                        		$zoneconfigs{'zone'}{@data[0]}{'io'}="/".@data[8]."/".@data[10].'.'.@data[9];
				}
				if(@data[11]){
                        		$zoneconfigs{'zone'}{@data[0]}{'iosec'}="/".@data[11]."/".@data[12].'.'.@data[13];
				}
                        	$zoneconfigs{'zone'}{@data[0]}{'levelsec'}=@data[16];
                        	$zoneconfigs{'zone'}{@data[0]}{'oper'}=@data[14];
                        	$zoneconfigs{'zone'}{@data[0]}{'desc'}=@data[15];
                        	$zoneconfigs{'zone'}{@data[0]}{'set'}=@data[4];
                        	$zoneconfigs{'zone'}{@data[0]}{'sethigh'}=@data[5];
                        	$zoneconfigs{'zone'}{@data[0]}{'hyst'}=@data[6];
                        	$zoneconfigs{'zone'}{@data[0]}{'prog'}=@data[7];
				push(@zones, @data[0]);
                                print "Temp device @data[0] : $zoneconfigs{'zone'}{@data[0]}{'name'} $zoneconfigs{'zone'}{@data[0]}{'address'} $zoneconfigs{'zone'}{@data[0]}{'path'} $zoneconfigs{'zone'}{@data[0]}{'io'} $zoneconfigs{'zone'}{@data[0]}{'desc'} found \n" if $debug;
				$device=1;
			}
        	}
		if($device eq 0){
			$deviceserror=@data[2]. ' '.@data[1]." not found\n" if $debug;
		 	print @data[2]. ' '.@data[1]." not found\n" if $debug;
		}else{
		 	print @data[2]. ' '.@data[1]." is found\n" if $debug;
		}
	}
	return(@zones);
}
sub owfs_ventilation_init(){
#+----+-----------------+------+---------+-----------+-------+---------+---------+------------+-----------+-----------+-----------------+--------+----------+-------+------+--------+
#| id | name            | zone | settemp | hystersis | iodev | iodevnr | extzone | extsettemp | tempraise | operation | description     | iodev2 | iodevnr2 | sleep | rise | sleep2 |
#+----+-----------------+------+---------+-----------+-------+---------+---------+------------+-----------+-----------+-----------------+--------+----------+-------+------+--------+
#|  3 | Kas ventilatie1 |   20 | 25      | 1         |    36 | 6       |    NULL | NULL       | NULL      |         2 | Kas ventilatie1 |     36 | 7        |     2 |    5 |      5 |
#+----+-----------------+------+---------+-----------+-------+---------+---------+------------+-----------+-----------+-----------------+--------+----------+-------+------+--------+

	my @zones;
	my $query = "SELECT v.id AS id, v.name AS name, z.zone AS zone, v.settemp AS settemp, v.hystersis AS hystersis, d.name AS namedev, d.device AS iodevice, d.file AS iofile, d2.name AS namedev2, d2.device AS iodevice2, d2.file AS iofile2, v.iodevnr AS iodevnr, v.iodevnr2 AS iodevnr2, v.sleep as sleep,v.sleep2 as sleep2,v.rise as rise,v.operation AS operation, v.wind as wind,v.description AS description FROM ventilation v LEFT OUTER JOIN zone z ON v.zone=z.id LEFT OUTER JOIN 1wdevices d ON v.iodev=d.id LEFT OUTER JOIN 1wdevices d2 ON v.iodev2=d2.id";
#0         id: 3
#1       name: Kas ventilatie1
#2       zone: Kastuin
#3    settemp: 25
#4  hystersis: 1
#5    namedev: piokastuin
#6   iodevice: 29.CFC922000000
#7     iofile: PIO
#8   namedev2: piokastuin
#9  iodevice2: 29.CFC922000000
#10    iofile2: PIO
#11    iodevnr: 6
#12   iodevnr2: 7
#13      sleep: 2
#14     sleep2: 5
#15       rise: 5
#16  operation: 2
#17  wind: 2
#18 description: Kas ventilatie1


	my @owaddresses=(split(/\s+/,$owaddress));
	my $connection = connectmysqldb();
	 my $statement = $connection->prepare($query);
	$statement->execute();
   	while (my @data = $statement->fetchrow_array()) {
       		for my $address (@owaddresses){
                	if(OWNet::read( $address , "/".@data[6]."/type" )){
	#			print "$address @data[1] $address /@data[2]/type\n" if $debug;
                       		$zoneconfigs{'ventilation'}{@data[0]}{'id'}=$data[0];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'name'}=$data[2];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'zone'}=$data[2];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'set'}=@data[3];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'hyst'}=@data[4];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'address'}=$address;
				if(@data[6]){
                       			$zoneconfigs{'ventilation'}{@data[0]}{'io'}="/".@data[6]."/".@data[7].'.'.@data[11];
				}
				if(@data[9]){
                       			$zoneconfigs{'ventilation'}{@data[0]}{'io2'}="/".@data[9]."/".@data[10].'.'.@data[12];
				}
                       		$zoneconfigs{'ventilation'}{@data[0]}{'sleep'}=@data[13];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'sleep2'}=@data[14];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'rise'}=@data[15];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'oper'}=@data[16];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'wind'}=@data[17];
                       		$zoneconfigs{'ventilation'}{@data[0]}{'desc'}=@data[18];
				push(@zones, @data[0]);
                        	print "Vent io @data[0] : $zoneconfigs{'ventilation'}{@data[0]}{'name'} $zoneconfigs{'ventilation'}{@data[0]}{'address'} $zoneconfigs{'ventilation'}{@data[0]}{'io'} $zoneconfigs{'ventilation'}{@data[0]}{'io2'} $zoneconfigs{'ventilation'}{@data[0]}{'desc'} found\n" if $debug;
			}
        	}
	}
	return(@zones);
}
sub owfs_timer_init(){
# 0 t.id AS id
# 1 t.name AS name
# 2 t.starttime AS starttime
# 3 t.endtime AS endtime
# 4 d.name AS namedev
# 5 d.device AS iodevice
# 6 d.file AS iofile
# 7 t.iodevnr AS iodevnr
# 8 t.zone AS sensorid
# 9 t.operation AS operation
# 10  t.description AS description
# 11 t.duration AS duration

	my @zones;
	my $query = "SELECT t.id AS id, t.name AS name, t.starttime AS starttime, t.endtime AS endtime, d.name AS namedev, d.device AS iodevice, d.file AS iofile, t.iodevnr AS iodevnr, t.zone AS sensorid,t.operation AS operation, t.description AS description, t.duration AS duration, t.zonevalue as zonevalue FROM timer t LEFT OUTER JOIN 1wdevices d ON t.iodev=d.id LEFT OUTER JOIN sensor z ON t.zone=z.id;";
	my @owaddresses=(split(/\s+/,$owaddress));
	my $connection = connectmysqldb();
	 my $statement = $connection->prepare($query);
	$statement->execute();
   	while (my @data = $statement->fetchrow_array()) {
       		for my $address (@owaddresses){
			if(OWNet::read( $address , "/".@data[5]."/type" )){
				print "IO $address @data[1] $address /@data[5]/type\n" if $debug;
                        	$zoneconfigs{'timer'}{@data[0]}{'address'}=$address;
                       		$zoneconfigs{'timer'}{@data[0]}{'id'}=$data[0];
                       		$zoneconfigs{'timer'}{@data[0]}{'name'}=$data[1];
                       		$zoneconfigs{'timer'}{@data[0]}{'start'}=$data[2];
                       		$zoneconfigs{'timer'}{@data[0]}{'end'}=@data[3];
                       		$zoneconfigs{'timer'}{@data[0]}{'sensorid'}=@data[8];
				if(@data[4]){
                       			$zoneconfigs{'timer'}{@data[0]}{'io'}="/".@data[5]."/".@data[6].'.'.@data[7];
				}
                       		$zoneconfigs{'timer'}{@data[0]}{'oper'}=@data[9];
                       		$zoneconfigs{'timer'}{@data[0]}{'desc'}=@data[10];
                                $zoneconfigs{'timer'}{@data[0]}{'duration'}=@data[11];
                                $zoneconfigs{'timer'}{@data[0]}{'sensorvalue'}=@data[12];
				push(@zones, @data[0]);
                        	print "Timer @data[0] : $zoneconfigs{'timer'}{@data[0]}{'name'} $zoneconfigs{'timer'}{@data[0]}{'address'} $zoneconfigs{'timer'}{@data[0]}{'io'} $zoneconfigs{'timer'}{@data[0]}{'desc'} found\n" if $debug;
			}
        	}
	}
	return(@zones);
}


sub owfs_sensor_init(){
        my @zones;
        my @data;
        my $query = "SELECT s.id AS id, s.name AS name, s.setvalue AS setvalue, d.name AS namedev, d.device AS iodevice, d.file AS iofile, s.iodevnr AS iodevnr, s.description AS description,sensortype as sensortype FROM sensor s LEFT OUTER JOIN 1wdevices d ON s.iodev=d.id;";
#       | id0 | name1   | setvalue2 | namedev3 | iodevice4        | iofile5 | iodevnr6 | description7 | sensortype8
        my @owaddresses=(split(/\s+/,$owaddress));
        my $connection = connectmysqldb();
         my $statement = $connection->prepare($query);
        $statement->execute();
        while (@data = $statement->fetchrow_array()) {
                my $device=0;
                for my $address (@owaddresses){
                        if(OWNet::read( $address , "/".@data[4]."/type" )){
                                $zoneconfigs{'sensor'}{@data[0]}{'address'}=$address;
                                $zoneconfigs{'sensor'}{@data[0]}{'id'}=@data[0];
                                $zoneconfigs{'sensor'}{@data[0]}{'name'}=@data[1];
                                $zoneconfigs{'sensor'}{@data[0]}{'set'}=@data[2];
                                if(@data[6]){
                                        $zoneconfigs{'sensor'}{@data[0]}{'path'}="/".@data[4]."/".@data[5].'.'.@data[6];
                                }else{
                                        $zoneconfigs{'sensor'}{@data[0]}{'path'}="/".@data[4]."/".@data[5];
                                }
                                $zoneconfigs{'sensor'}{@data[0]}{'desc'}=@data[7];
                                $zoneconfigs{'sensor'}{@data[0]}{'sensortype'}=@data[8];
                                push(@zones, @data[0]);
                                print "Sensor @data[0] : $zoneconfigs{'sensor'}{@data[0]}{'name'} $zoneconfigs{'sensor'}{@data[0]}{'address'} $zoneconfigs{'sensor'}{@data[0]}{'path'} $zoneconfigs{'sensor'}{@data[0]}{'desc'} sensortype $zoneconfigs{'sensor'}{@data[0]}{'sensortype'} found\n" if $debug;
                                $device=1;
                        }else{
                                $zoneconfigs{'sensor'}{@data[0]}{'id'}=@data[0];
                                $zoneconfigs{'sensor'}{@data[0]}{'name'}=@data[1];
                                $zoneconfigs{'sensor'}{@data[0]}{'desc'}=@data[7];
                                $zoneconfigs{'sensor'}{@data[0]}{'sensortype'}=@data[8];
                                print "Sensor @data[0] : $zoneconfigs{'sensor'}{@data[0]}{'name'} $zoneconfigs{'sensor'}{@data[0]}{'desc'} sensortype $zoneconfigs{'sensor'}{@data[0]}{'sensortype'} found\n" if $debug;
			}
                }
        }
        return(@zones);
}

sub zone_starttimerdb(){
        my $zoneid=shift;
        my $time=shift;
        my $timer=0;
        my $query = "insert into zonetimer (zoneid,starttime,time) values ('$zoneid','$time','$timer')";
#       print "$query\n";
        my $connection = connectmysqldb();
        my $statement = $connection->prepare($query);
        $statement->execute();
        return($statement->{mysql_insertid});
}
sub zone_endtimerdb(){
        my $id=shift;
        my $timer=shift;
        my $query = "UPDATE zonetimer SET time='$timer' WHERE id='$id'";
#       print "$query\n";
        my $connection = connectmysqldb();
        my $statement = $connection->prepare($query);
        $statement->execute();
}


sub connectmysqldb {
	open(ACCESS_INFO, "</usr/local/etc/.mysqlpwd") || die "Can't access login credentials";
	my $database = <ACCESS_INFO>;
	my $host = <ACCESS_INFO>;
	my $userid = <ACCESS_INFO>;
	my $passwd = <ACCESS_INFO>;
	close(ACCESS_INFO);
	chomp ($database, $host, $userid, $passwd);
	print "$database, $host, $userid, $passwd" if $debug;
	my $connectionInfo="dbi:mysql:$database;$host";
	my $l_connection = DBI->connect($connectionInfo,$userid,$passwd);
	return $l_connection;
}

sub rsyslog(){
     setlogsock("unix") or print "setlogsock: $!";
     openlog('owfs', 'ndelay', 'user');
     syslog('info', "$log");
     closelog();
}

sub signal_handler {
        print "reading process huis canceld!!\n";
        exit;
}


