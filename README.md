
# One wire greenhouse or other home automation.

This software has been originated from the need of controlling my greenhouse with tropical plants and animals.   (this is a rewrite from perl to python)

It consists of a php Drupal/Backdrop module as frontend and a python script with owfs software for the 1-wire devices as backend.

Doing:
* Database of the 1-wire devices
* Controlling temperatures
* Timers which can have dependencies of sensors
* Sensors
* Creating graphs with the use of rrdtools

Lots of information of the 1-wire devices and the OWFS software can be found at https://owfs.org

## Requirements

* Hardware like a Raspberry PI or Zero, Beaglebone black etc. with a 1-wire interface bus. Devices like the temperature sensor  DS18B20 etc.
* OWFS software on this hardware.
* Mysql database
* Apache/nginx webserver for the Drupal/Backdrop CMS and status page.


## Hardware and owfs installation
* https://www.maximintegrated.com/en/app-notes/index.mvp/id/1796
* https://owfs.org
* https://fstab.nl/en/Raspberry-Pi-zero
* https://fstab.nl/en/beaglebone


## Software
* Installation on the 1-wire controlling hardware
  * apt-get install owfs ow-shell owserver python python-pip mysql-server git python-mysqldb mysql-client
  * pip install pyownet
  * pip install astral
  * pip install tabulate
  * as a normal user: git clone https://gitlab.com/ulterius/owcontrol.git
* Configuration on the 1-wire controlling hardware
  * Create a mysql database forexample: on the mysql prompt: create database control;
  * Grant a mysql user on localhost and your subnet: GRANT ALL PRIVILEGES on control.* to 'dbuser'@'localhost' IDENTIFIED by 'dbpassword';
  * GRANT ALL PRIVILEGES on control.* to 'dbuser'@'192.168.0.%' IDENTIFIED by 'dbpassword';
  * Import the database:  mysql control <control.sql
* Frontend Drupal/Backdrop CMS
  * Install the cms, you will find the Howto's on https://drupal.org or https://backdrop.org (backdrop is a spinoff from Drupal 7)
  * Copy the cms module in the modules directory and  activate the module.
  * The CMS has it's own mysql database, the 1-wire database is an external db.  The CMS will ask for the parameters.
* Frontend python cli script:  ./control_admin.py  -f config.ini
* Testing
  * Besure you see your 1-wire devices with: owdir
  * Can you connect with mysql  to the control database from the CMS frontend to the the 1-wire controlling hardware: mysql -udbuser -p -h1wire-host
* Running the python backend script on the 1-wire controlling hardware
  * Look at the config.ini first!
  * running: ./control.py  -d -f config.ini  (-d debug foreground)

##Perl version, control.pl, need some TLC to get it work in your environment.
See at the wiki for more information.

https://gitlab.com/ulterius/owcontrol/wikis/home

Nico Bouthoorn
nico@ulterius.nl
