<?php

date_default_timezone_set("Europe/Amsterdam");
//Database id of the temperature zone
$id='12';
//OWFS server
$owserver=bb01;
//OWFS port
$owport=4304;
//$URL=$_SERVER['SERVER_NAME'];
$URL="cv-test.home.cuora.nl";
//Mysql host
$dbhost="bb01";
//MYSQL database
$db="control";
//MYSQL user
$dbuser="control";
//MYSQL password
$dbpassword="control";


// Create connection
$con=mysqli_connect($dbhost,$dbuser,$dbpassword,$db);

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
//Get the the 1wire device
$result=mysqli_query($con,"SELECT d.device FROM zone z LEFT OUTER JOIN 1wdevices d ON z.device=d.id where z.id='$id'");
$row = mysqli_fetch_array($result);
$device=$row['device'];
//Read the temperature
$temperature=exec("owread  -s ". $owserver.":".$owport." /". $device."/temperature");
$temperature=sprintf("%.1f",$temperature); 
if(empty($temperature)){
	print "Unable to read the temperature, "."owread  -s ". $owserver.":".$owport." /". $device."/temperature";
}
//Try to update the temperature setting
if(!empty($_GET['submit'])) {
	$settemp=$_GET['amount'];
	mysqli_query($con,"UPDATE zone SET settemp=$settemp WHERE id='$id'");
	header('Location: http://'.$URL);
}
$result=mysqli_query($con,"select settemp from zone where id='$id'");
$row = mysqli_fetch_array($result);
$amount=$row['settemp'];
mysqli_close($con);
//Heating?
if ($temperature < $amount) {
	$heateron="<img src=\"images/flame.png\">";
}else{
	$heateron="";
}

print "<!doctype html>
<html lang=\"en\">
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"refresh\" content=\"10;url=http://".$URL.$_SERVER['REQUEST_URI']."\" />
 <title>Home</title>
<link rel=\"shortcut icon\" href=\"images/cv.png\" type=\"image/x-icon\">
<link rel=\"stylesheet\" href=\"css/jquery-ui.css\">
 <link rel=stylesheet href=css/huis.css> 
<script src=\"js/jquery-1.10.2.js\"></script>
<script src=\"js/jquery-ui.js\"></script>
<script src=\"js/jquery.ui.touch-punch.min.js\"></script>
<script>
$(function() {
$( \"#slider\" ).slider({
value:".$amount.",
min: 5,
max: 30,
step: .5,
slide: function( event, ui ) {
$( \"#amount\" ).val( ui.value );
}
});
$( \"#amount\" ).val( $( \"#slider\" ).slider( \"value\" ) );
});
</script>
<script>
$(document).ready(function(){
  $(\"form\").submit(function(){
  });
});
</script>
</head>
<body>
 <div id=page-wrap>
 <h1>Home ".$temperature."C".$heateron."</h1> 
<form method=get action=".$_SERVER['PHP_SELF']."> 
<p>
<center>
<input type=\"text\" id=\"amount\" name=\"amount\"  style=\"border:1px; color:#f6931f; font-weight:bold; font-size:3em;\">
</p>
<div id=\"slider\"></div>
<div class=buttons>
 <input type=submit value=Set id=submit name=submit> 
</div>
<br>
".date('j/n/Y, g:i a')."
</center>
</div>
</body>
</html>";
?>
