#!/usr/bin/python
import sys
import time
import argparse
import os
import ConfigParser
import owcontrol


parser = argparse.ArgumentParser(
     prog='PROG',
     description='''Control owfs program
         '''
)
   
parser.add_argument('-f', type=str, help='file config name')
parser.add_argument('-d', help='Debug', action='store_true')
parser.add_argument('-l', type=str, help='file log name')

args = parser.parse_args()

configfile = args.f
if os.path.isfile(configfile):

	config =  ConfigParser.ConfigParser()
	config.read(configfile)
	vars={}
	vars["loop"]=float(config.get("DEFAULT", "loop"))
	on=config.get("OWFS", "on")
        vars["on"]=owcontrol.strtobin(on)
	off=config.get("OWFS", "off")
        vars["off"]=owcontrol.strtobin(off)
	vars["owhosts"]=config.get("OWFS", "owhosts")
	vars["vdd"]=config.get("OWFS", "vdd")
	vars["city_name"] =config.get("DAYLIGHT", "city_name")
	vars["daylightcorrection"] =int(config.get("DAYLIGHT", "daylightcorrection"))
	vars["dbhost"]=config.get("DATABASE", "dbhost")
	vars["dbuser"]=config.get("DATABASE", "dbuser")
	vars["dbpassword"]=config.get("DATABASE", "dbpassword")
	vars["database"]=config.get("DATABASE", "database")
	vars["rrd"]=config.get("RRD", "rrd")
	vars["rrdhost"]=config.get("RRD", "rrdhost")
	vars["rrdport"]=int(config.get("RRD", "rrdport"))
	vars["rrddbpath"]=config.get("RRD", "rrddbpath")
	vars["rrddbcreate"]=config.get("RRD", "rrddbcreate")
	vars["rrdimage"]=config.get("RRD", "rrdimage")
	vars["rrdimagepath"]=config.get("RRD", "rrdimagepath")
	vars["statusfile"]=config.get("FILES", "statusfile")
else:
	print "File doesn't exists:" + configfile
	sys.exit(2)

if args.d:
   print "debug on"
   vars["debug"]=True
else:
   if args.l:
       sys.stderr = open(args.l, 'w')
   vars["debug"]=False
   pid = os.fork()
   if pid :
     print pid
     sys.exit()

while True:
    beforetime=float(time.time())
    vars=owcontrol.temperature_zones(vars)
    vars=owcontrol.sensor_zones(vars)
    vars=owcontrol.timer_zones(vars)
    vars=owcontrol.ventilation_zones(vars)
    owcontrol.output(vars)
    nowtime=float(time.time())
    sleep=vars["loop"] - (nowtime - beforetime)
    if sleep <0:
        sleep=0
    time.sleep( sleep )
