#!/usr/bin/python
import sys
import time
import argparse
import os
import ConfigParser
import MySQLdb as mdb
import datetime
from tabulate import tabulate
import readline



parser = argparse.ArgumentParser(
     prog='PROG',
     description='''Control owfs admin cli
         '''
)
   
parser.add_argument('-f', type=str, help='file config name')

args = parser.parse_args()

if args.f:
   configfile = args.f
else:
   configfile = 'config.ini'

if os.path.isfile(configfile):

	config =  ConfigParser.ConfigParser()
	config.read(configfile)
	vars={}
	vars["dbhost"]=config.get("DATABASE", "dbhost")
	vars["dbuser"]=config.get("DATABASE", "dbuser")
	vars["dbpassword"]=config.get("DATABASE", "dbpassword")
	vars["database"]=config.get("DATABASE", "database")
else:
	print "File doesn't exists:" + configfile
	sys.exit(2)


def dbquery(vars,query,select=''):
    try:
	con = mdb.connect(vars["dbhost"], vars["dbuser"], vars["dbpassword"], vars["database"])
	with con:
		cur = con.cursor(mdb.cursors.DictCursor)
	cur.execute(query)
	
	if select:
		rows = cur.fetchone()
	else:
		rows = cur.fetchall()
        return rows
    except ValueError as ve:
        print(ve)

def dbquery_print(vars,query):
    try:
	con = mdb.connect(vars["dbhost"], vars["dbuser"], vars["dbpassword"], vars["database"])
	cur = con.cursor()
	cur.execute(query)
	
	header = [str(i[0]) for i in cur.description]
	print(tabulate(cur,headers=header,tablefmt="fancy_grid"))
    except ValueError as ve:
        print(ve)

def dbquery_update(vars,query):
    try:
	con = mdb.connect(vars["dbhost"], vars["dbuser"], vars["dbpassword"], vars["database"])
	cur = con.cursor()
	cur.execute(query)
        con.commit()
        print(cur.rowcount, "record(s) affected/inserted") 
    except ValueError as ve:
        print(ve)

def check_device_exists(vars,id):
    query="SELECT id from 1wdevices where id=" + str(id)
    row=dbquery(vars,query,select=True)
    return row

def list_devices(vars):
    query="SELECT id,device,type,name from 1wdevices order by type,id"
    dbquery_print(vars,query)

def rlinput(prompt, prefill=''):
#default_value = "an insecticide"
#stuff = rlinput("Caffeine is: ", default_value)
#print("final answer: " + stuff)
   readline.set_startup_hook(lambda: readline.insert_text(prefill))
   try:
      return raw_input(prompt)
   finally:
      readline.set_startup_hook()


def get_float(vars,default='',textinput=''):
    if not default: default=float(0)
    subans=True
    while subans:
        value=None
        #value=raw_input(textinput)
        value=rlinput(textinput, default)
        if not value:
            value=float(default)
            subans=None
        else:
            try:
                value=float(value)
            except ValueError as ve:
                print(ve)
            else:
                subans=None
    return value

def get_int(vars,default='',textinput=''):
    if not default: default=int(0)
    subans=True
    while subans:
        value=None
        #value=raw_input(textinput)
        value=rlinput(str(textinput), default)
        if not value:
            value=int(default)
            subans=None
        else:
            try:
                value=int(value)
            except ValueError as ve:
                print(ve)
            else:
                subans=None
    return value

def get_str(vars,default='',textinput=''):
    subans=True
    while subans:
        value=None
#        value=raw_input(textinput)
        value=rlinput(textinput, default)
        if not value:
            value=str(default)
            subans=None
        else:
            try:
                value=str(value)
            except ValueError as ve:
                print(ve)
            else:
                subans=None
    return value

def get_device(vars,default='',textinput=''):
    if not default: default=int(99999)
    if default == 'None' or  default=='':
	default=0
    subans=True
    while subans:
        value=None
        value=get_int(vars,default,textinput)
        if not value==0:
	   if check_device_exists(vars,value):
             subans=None
           else:
             print("Device doesn't exist")
        else:
	   subans=None
    return value

def get_serialnr(vars,default='',textinput=''):
    subans=True
    while subans:
        value=None
        value=get_str(vars,default,textinput)
        if len(value) == 15:
            subans=None
        else:
            print("Not a valid serial number")
    return value

def get_operation(vars,default='',textinput=''):
    if not default: default=int(0)
    subans=True
    while subans:
        value=get_int(vars,default,str(textinput))
        if value in (0,1,2):
            subans=None
        else:
            print("0=off,1=on,2=auto")
    return value

def get_logical(vars,default='',textinput=''):
    subans=True
    while subans:
        value=get_str(vars,default,str(textinput))
        if value in ('<', '>'):
            subans=None
        else:
            print("< for less  or > for measured value is greater then set")
    return value

def get_sensortype(vars,default='',textinput=''):
    print_sensor_types()
    listtypes=sensor_types()
    keys = list(listtypes.keys())

    subans=True
    while subans:
        value=get_int(vars,default,textinput)
        if str(value) in keys:
            subans=None
        else:
            print(keys)
    return value

def get_time(vars,default='',textinput=''):
    subans=True
    while subans:
        time=get_str(vars,default,str(textinput))
        if validate_time(time):
            subans=None
            value=get_sec(time)
        else:
            print("Enter HH:MM eg 13:10")
    return value

def validate_time(time):
    try:
        validtime = datetime.datetime.strptime(time, "%H:%M")
        return True
    except ValueError:
        return False

def devices_list():
    list=[
            ["DS18B20", "temperature","DS18B20 Programmable Resolution 1-Wire Digital Thermometer"],
            ["DS2408",  "PIO","DS2408 1-Wire 8-Channel Addressable Switch"],
            ["DS2450",  "volt","DS2450 1-Wire Quad A/D Converter"],
            ["DS2438",  "volt or temperature","DS2438 Battery ID, Temperature, Voltage, Current Monitor with Real Time Clock"],
         ]
    return list

def sensor_types():
    list={}
    list[""]= ""
    list["1"]= "waterstorage"
    list["2"]= "light"
    list["3"]= "humidity"
    list["8"]= "VDD"
    list["9"]= "sunrise/sunset"
    return list

def operations(operation=''):
    if not operation:
	operation=str('0')
    list={}
    list['0']= "off"
    list['1']= "on"
    list['2']= "auto"
    return list[str(operation)]

def print_sensor_types():
    header=['ID','Description']
    listtypes=sensor_types()
    list=[]
    for key, value in listtypes.iteritems():
       temp=[key,value] 
       list.append(temp)
    print(tabulate(list,headers=header,tablefmt="fancy_grid"))


def devices_list_print():
    list=devices_list()
    header=["Type","File","Description"]
    print(tabulate(list,headers=header,tablefmt="fancy_grid"))

def daytime_sec():
    now = datetime.datetime.now()
    return int((now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds())

def datetimecustom():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d %H:%M")

def convertsec(seconds):
    return time.strftime("%H:%M", time.gmtime(seconds))

def get_sec(time_str):
    h, m = time_str.split(':')
    return int(h) * 3600 + int(m) * 60

def device_admin(vars):
    listquery="select id,device,type,name,file,description from 1wdevices"

    loop=True
    while loop:
        dbquery_print(vars,listquery)
        print "\n"
        ans=raw_input("Select id number to edit, n(ew), d(elete), q(uit)")
        if ans=="n":
            print("\n New")
            #Device
            textinput="Device name "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder=(value,)
            #Device type
            devices_list_print()
            rows=devices_list()
            type=str(" (")
            for row in rows:
                type+=row[0] + ", "
            textinput="Set device type" + type + "): "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Device 64bit id
            textinput="Set device 64bit serial nr: "
            value=str(get_serialnr(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #file
            file=str(" (")
            for row in rows:
                file+=row[1] + ", "
            textinput="Set file " + file + ") or add new one: "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Description
            textinput="Set description: "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)

            query="INSERT INTO 1wdevices (name,type,device,file,description) VALUES ('%s','%s','%s','%s','%s')"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="d":
            textinput="Give id number for device to delete: "
            id=str(get_int(vars,'',textinput))
            placeholder=(id,)
            query="DELETE FROM 1wdevices where id='%s'"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="q":
            loop = None
        else:
            try:
                value=int(ans)
                query=listquery + ' where id=' + ans
                row=dbquery(vars,query,select=True)
                if row:
                    #Device
                    textinput="Change device name :"
                    value=str(get_str(vars,str(row['name']),textinput))
                    print(value)
                    placeholder=(value,)
                    #Device type
                    devices_list_print()
                    rows=devices_list()
                    type=str(" (")
                    for rowdevice in rows:
                        type+=rowdevice[0] + ", "
                    textinput="Change device type" + type + ") " + ": "
                    value=str(get_str(vars,str(row['type']),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Device 64bit id
                    textinput="Change device 64bit serial nr :"
                    value=str(get_serialnr(vars,str(row['device']),textinput))
                    print(value)
                    placeholder+=(value,)
                    #file
                    file=str(" (")
                    for rowdevice in rows:
                        file+=rowdevice[1] + ", "
                    textinput="Change file " + file + ") " + ": "
                    value=str(get_str(vars,str(row['file']),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Description
                    textinput="Change description :"
                    value=str(get_str(vars,str(row['description']),textinput))
                    print(value)
                    placeholder+=(value,)
                    placeholder+=(ans,)
                    query="UPDATE 1wdevices SET name='%s' ,type ='%s' , device ='%s' , file ='%s' ,description='%s' where id='%s'"%(placeholder)
                    dbquery_update(vars,query) 
            except:
                print(ans)

def print_zone(vars):
    query="SELECT z.id AS id, z.zone AS zone, d.device AS device, z.settemp AS settemp, z.logical as logical, z.hystersis AS hystersis, d2.name as ioname,d2.device AS iodevice, d2.file AS iofile, z.iodevnr AS iodevnr, z.operation AS operation, z.description AS description FROM zone z LEFT OUTER JOIN 1wdevices d ON z.device=d.id LEFT OUTER JOIN 1wdevices d2 ON z.iodev=d2.id"
    header=['ID','Zone','Setvalue','Hystersis','Device','IO','Operation','Description']
    list=[]
    rows=dbquery(vars,query)
    for row in rows:
        sublist=[]
        sublist.append(row['id'])
        sublist.append(row['zone'])
        if row['logical']:
             sublist.append(row['logical'] + " " + row['settemp'])
        else:
            sublist.append('> ' + row['settemp'])
        sublist.append(row['hystersis'])
        sublist.append(row['device'])
        if row['iodevice']:
            sublist.append(str(row['ioname'])+"\n"+str(row['iodevice'])+'/'+str(row['iofile'])+'.'+str(row['iodevnr']))
        else:
            sublist.append('')
        sublist.append(operations(row['operation']))
        sublist.append(row['description'])
        list.append(sublist)

    print(tabulate(list,headers=header,tablefmt="fancy_grid"))

def list_zones(vars):
    query="SELECT id,zone FROM zone"
    header=['ID','Zone']
    list=[]
    rows=dbquery(vars,query)
    for row in rows:
        sublist=[]
        sublist.append(row['id'])
        sublist.append(row['zone'])
        list.append(sublist)
    print(tabulate(list,headers=header,tablefmt="fancy_grid"))

def zone_admin(vars):
    select="SELECT id, zone, device, settemp, hystersis, logical, iodev,iodevnr, operation,  description FROM zone where id=%s"

    loop=True
    while loop:
        print_zone(vars)
        print "\n"
        ans=raw_input("Select id number to edit, n(ew), d(elete), q(uit)")
        if ans=="n":
            print("\n New")
            list_devices(vars)
            print("\n")
            #Zone name
            textinput="Set zone name "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder=(value,)
            #Temp sensor
            textinput="Set device id number: "
            value=str(get_device(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Settemp
            textinput="Set temperature: "
            value=str(get_float(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Settemp
            textinput="Set < for heating or > for cooling: "
            value=str(get_logical(vars,'<',textinput))
            print(value)
            placeholder+=(value,)
            #hystersis
            textinput="Set hystersis (.2 C) : "
            value=str(get_float(vars,float('.2'),textinput))
            print(value)
            placeholder+=(value,)
            #IO device
            textinput="Set io device id number: "
            value=str(get_int(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #IO device port number
            textinput="Set io device port number: "
            value=str(get_int(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Operation, off, on or auto
            textinput="Set io operation, 0=off, 1=on, 2=auto: "
            value=str(get_operation(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Description
            textinput="Set description: "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)

            query="INSERT INTO zone (zone,device,settemp,logical,hystersis,iodev,iodevnr,operation,description) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="d":
            textinput="Give id number for zone to delete: "
            id=str(get_int(vars,'',textinput))
            placeholder=(id,)
            query="DELETE FROM zone where id='%s'"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="q":
            loop = None
        else:
            try:
                value=int(ans)
                placeholder=int(ans,)
                query=select%(placeholder)
                row=dbquery(vars,query,select=True)
                if row:
                    list_devices(vars)
                    #Zone name
                    textinput="Change zone name :"
                    value=str(get_str(vars,str(row["zone"]),textinput))
                    print value
                    placeholder=(value,)
                    #Temp sensor
                    textinput="Change device id number: "
                    value=str(get_device(vars,str(row["device"]),textinput))
                    print value
                    placeholder+=(value,)
                    #Settemp
                    textinput="Change temperature :" 
                    value=str(get_float(vars,str(row["settemp"]),textinput))
                    print value
                    placeholder+=(value,)
                    #Settemp
                    textinput="Is set temperature greater or smaller (> or <): "
                    value=str(get_logical(vars,str(row["logical"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #hystersis
                    textinput="Change hystersis :"
                    value=str(get_float(vars,str(row["hystersis"]),textinput))
                    print value
                    placeholder+=(value,)
                    #IO device
                    if row["iodev"]:
                        textinput="Change io device id number: "
                        value=str(get_device(vars,str(row["iodev"]),textinput))
                    else:
                        textinput="Set io device id number: "
                        value=str(get_device(vars,'',textinput))
                    print value
                    placeholder+=(value,)
                    #IO device port number
                    if row["iodevnr"]:
                        textinput="Change io device port number :"
                        value=str(get_int(vars,str(row["iodevnr"]),textinput))
                    else:
                        textinput="Set io device port number :"
                        value=str(get_int(vars,'0',textinput))
                    print value
                    placeholder+=(value,)
                    #Operation, off, on or auto
                    textinput="Change io operation, 0=off, 1=on, 2=auto :"
                    value=str(get_operation(vars,str(row["operation"]),textinput))
                    print value
                    placeholder+=(value,)
                    #Description
                    textinput="Change description: "
                    value=str(get_str(vars,row["description"],textinput))
                    print(value)
                    placeholder+=(value,)
                    placeholder+=(str(ans),)
                    query="UPDATE zone SET zone='%s' ,device ='%s' , settemp ='%s',logical='%s', , hystersis ='%s' ,iodev ='%s',iodevnr='%s',operation='%s',description='%s' where id='%s'"%(placeholder)
                    dbquery_update(vars,query) 
            except:
                print(ans)

def print_sensors(vars):
    query="SELECT s.id AS id, s.name AS name, s.setvalue AS setvalue, s.sensortype AS sensortype, s.hystersis AS hystersis, s.calibration AS calibration, s.devicenr AS devicenr, s.iodevnr AS iodevnr, s.operation AS operation, s.description AS description, d.name AS namedevice, d.device AS device, d.file AS devicefile, d2.name AS ioname, d2.device AS iodevice, d2.file AS iofile FROM sensor s LEFT OUTER JOIN 1wdevices d ON s.device=d.id LEFT OUTER JOIN 1wdevices d2 ON s.iodev=d2.id"
    sensortypes=sensor_types()
    header=['ID','Name','Setvalue','Hystersis','Calibration','Device','Type','IO','Operation','Description']
    list=[]
    rows=dbquery(vars,query)
    for row in rows:
        sublist=[]
        sublist.append(row['id'])
        sublist.append(row['name'])
        sublist.append(row['setvalue'])
        sublist.append(row['hystersis'])
        sublist.append(row['calibration'])
        if not row['devicenr']:
            sublist.append(str(row['namedevice'])+"\n"+str(row['device'])+'/'+str(row['devicefile']))
        elif not row['devicefile']: 
            sublist.append(str(row['namedevice'])+"\n"+str(row['device']))
        elif not row['device']: 
            sublist.append(str(row['namedevice']))
        else:
            sublist.append(str(row['namedevice'])+"\n"+str(row['device'])+'/'+str(row['devicefile'])+'.'+str(row['devicenr']))
        sublist.append(sensortypes[str(row['sensortype'])])
        if row['iodevice']:
            sublist.append(str(row['ioname'])+"\n"+str(row['iodevice'])+'/'+str(row['iofile'])+'.'+str(row['iodevnr']))
        else:
            sublist.append('')
        sublist.append(operations(row['operation']))
        sublist.append(row['description'])
        list.append(sublist)

    print(tabulate(list,headers=header,tablefmt="fancy_grid"))

def list_sensors(vars):
    query="SELECT id,name FROM sensor"
    sensortypes=sensor_types()
    header=['ID','Name']
    list=[]
    rows=dbquery(vars,query)
    for row in rows:
        sublist=[]
        sublist.append(row['id'])
        sublist.append(row['name'])
        list.append(sublist)
    print(tabulate(list,headers=header,tablefmt="fancy_grid"))


def sensor_admin(vars):
    select="SELECT name, setvalue, sensortype, hystersis, calibration,device, devicenr, iodevnr, operation, iodev , description FROM sensor where  id=%s"

    loop=True
    while loop:
        print_sensors(vars)
        print "\n"
        ans=raw_input("Select id number to edit, n(ew), d(elete), q(uit)")
        if ans=="n":
            print("\n New")
            print("\n")
            #Sensor name
            textinput="Set sensor name "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder=(value,)
            #sensor device
            list_devices(vars)
            textinput="Set device id number: "
            value=str(get_device(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Set Value
            textinput="Set value: "
            value=str(get_float(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Sensortype
            textinput="Set sensor type: "
            value=str(get_sensortype(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #hystersis
            textinput="Set hystersis (1) : "
            value=str(get_float(vars,float('1'),textinput))
            print(value)
            placeholder+=(value,)
            #Calibration
            textinput="Set calibration factor of the sensor : "
            value=str(get_float(vars,float('.2'),textinput))
            print(value)
            placeholder+=(value,)
            #IO device
            textinput="Set io device id number: "
            value=str(get_int(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #IO device port number
            textinput="Set io device port number: "
            value=str(get_int(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Operation, off, on or auto
            textinput="Set io operation, 0=off, 1=on, 2=auto: "
            value=str(get_operation(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Description
            textinput="Set description: "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            query="INSERT INTO sensor (name,device,setvalue,sensortype,hystersis,calibration, iodev, iodevnr,operation,description) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="d":
            textinput="Give id number for sensor to delete: "
            id=str(get_int(vars,'',textinput))
            placeholder=(id,)
            query="DELETE FROM sensor where id='%s'"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="q":
            loop = None
        else:
            try:
                value=int(ans)
                placeholder=int(ans,)
                query=select%(placeholder)
                row=dbquery(vars,query,select=True)
                if row:
                    textinput="Change sensor " + "(" + str(row["name"]) + "): "
                    value=str(get_str(vars,row["name"],textinput))
                    print(value)
                    placeholder=(value,)
                    #Temp sensor
                    list_devices(vars)
                    textinput="Change device id number " + "(" + str(row["device"]) + "): "
                    value=str(get_device(vars,str(row["device"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Settemp
                    textinput="Change value " + "(" + str(row["setvalue"]) + "): "
                    value=str(get_float(vars,row["setvalue"],textinput))
                    print(value)
                    placeholder+=(value,)
                    #Sensortype
                    textinput="Change sensor type " + "(" + str(row["sensortype"]) + "): "
                    value=str(get_sensortype(vars,str(row["sensortype"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #hystersis
                    textinput="Change hystersis (1)  " + "(" + str(row["hystersis"]) + "): "
                    value=str(get_float(vars,row["hystersis"],textinput))
                    print(value)
                    placeholder+=(value,)
                    #Calibration
                    textinput="Change calibration factor of the sensor : " + "(" + str(row["calibration"]) + "): "
                    value=str(get_float(vars,row["calibration"],textinput))
                    print(value)
                    placeholder+=(value,)
                    #IO device
                    textinput="Change io device id number:"
                    value=str(get_device(vars,str(row["iodev"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #IO device port number
                    textinput="Change io device port number: "
                    value=str(get_int(vars,str(row["iodevnr"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Operation, off, on or auto
                    textinput="Change io operation, 0=off, 1=on, 2=auto :"
                    value=str(get_operation(vars,str(row["operation"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Description
                    textinput="Change description :"
                    value=str(get_str(vars,row["description"],textinput))
                    print(value)
                    placeholder+=(value,)
                    placeholder+=(str(ans),)
                    query="UPDATE sensor SET name='%s' ,device ='%s' , setvalue ='%s' ,sensortype='%s', hystersis ='%s' ,calibration='%s',iodev ='%s',iodevnr='%s',operation='%s',description='%s' where id='%s'"%(placeholder)
                    print(query)
                    dbquery_update(vars,query) 
            except ValueError as ve:
                print(placeholder)
                print(ve)
            
#            except:
#                print(ans)

def print_timers(vars):
    query="SELECT t.id AS id, t.name AS name, t.starttime AS starttime, t.endtime AS endtime, d.name AS namedev, d.device AS iodevice, d.file AS iofile, t.iodevnr AS iodevnr, t.sensor AS sensor, t.zone AS zone, s.name AS sensorname, s.sensortype AS sensortype, z.zone AS zonename, t.value AS value, t.logical AS logical,t.operation AS operation, t.description AS description FROM timer t LEFT OUTER JOIN 1wdevices d ON t.iodev=d.id LEFT OUTER JOIN sensor s ON t.sensor=s.id LEFT OUTER JOIN zone z ON t.zone=z.id"
    sensortypes=sensor_types()
    header=['ID','Name','Start time','End time','IO','Dependencies','Sensor','Sensor value','Operation','Description']
    list=[]
    rows=dbquery(vars,query)
    for row in rows:
        sublist=[]
        sublist.append(row['id'])
        sublist.append(row['name'])
        sublist.append(convertsec(row['starttime']))
        sublist.append(convertsec(row['endtime']))
        sublist.append(str(row['namedev'])+"\n"+str(row['iodevice'])+'/'+str(row['iofile'])+'.'+str(row['iodevnr']))
        if row['sensor']:
            sensortypes=sensor_types()
            sublist.append(sensortypes[str(row['sensortype'])])
            sublist.append(row['sensorname'])
        elif row['zone']:
            sublist.append('Temperature')
            sublist.append(row['zonename'])
        else:
            sublist.append('')
            sublist.append('')
        sublist.append(row['logical'] + row['value'])
        sublist.append(operations(row['operation']))
        sublist.append(row['description'])
        list.append(sublist)

    print(tabulate(list,headers=header,tablefmt="fancy_grid"))

def timer_admin(vars):
    select="SELECT id, name, starttime, endtime, iodev, iodevnr, sensor, zone, value,logical,operation, description FROM timer where id=%s"

    loop=True
    while loop:
        print_timers(vars)
        print "\n"
        ans=raw_input("Select id number to edit, n(ew), d(elete), q(uit)")
        if ans=="n":
            print("\n New")
            print("\n")
            #Timer name
            textinput="Set timer name "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder=(value,)
            #Start time to switch on
            textinput="Set start time for on (HH:MM): "
            value=str(get_time(vars,str('00:00'),textinput))
            print(value)
            placeholder+=(value,)
            #End time
            textinput="Set end time for off (HH:MM): "
            value=str(get_time(vars,str('00:00'),textinput))
            print(value)
            placeholder+=(value,)
            #Operation, off, on or auto
            textinput="Set io operation, 0=off, 1=on, 2=auto: "
            value=str(get_operation(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #io device
            list_devices(vars)
            textinput="Set io device id number: "
            value=str(get_device(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #IO device port number
            textinput="Set io device port number: "
            value=str(get_int(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            print "If using dependency, choose sensor or zone, not both"
            subans=True
            while subans:
                print "Temperature zones"
                list_zones(vars)
                textinput="Set zone id: "
                zoneid=get_int(vars,'',textinput)
                print "OR sensor"
                list_sensors(vars)
                textinput="Sensor id: "
                sensorid=get_int(vars,'',textinput)
                if zoneid and sensorid:
                    print "If using dependency, choose sensor or zone, not both"
                else:
                    if zoneid:
                        value=zoneid
                        placeholder+=(zoneid,)
                        placeholder+=(str('0'),)
                    elif sensorid:
                        placeholder+=(str('0'),)
                        placeholder+=(sensorid,)
                    else:
                        placeholder+=(str('0'),)
                        placeholder+=(str('0'),)
                    subans=None
            #Set logical
            textinput="Is set value greater or smaller (> or <): "
            value=str(get_logical(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            #Set Value
            textinput="Set value: "
            value=str(get_float(vars,'',textinput))
            print(value)
            placeholder+=(value,)

            #Description
            textinput="Set description: "
            value=str(get_str(vars,'',textinput))
            print(value)
            placeholder+=(value,)
            query="INSERT INTO timer (name, starttime, endtime, operation,iodev,iodevnr,zone, sensor, logical,value, description) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="d":
            textinput="Give id number for timer to delete: "
            id=str(get_int(vars,'',textinput))
            placeholder=(id,)
            query="DELETE FROM timer where id='%s'"%(placeholder)
            dbquery_update(vars,query) 
        elif ans=="q":
            loop = None
        else:
            try:
                placeholder=int(ans,)
                query=select%(placeholder)
                row=dbquery(vars,query,select=True)
                if row:
                    #Timer name
                    textinput="Change timer name "
                    value=str(get_str(vars,str(row["name"]),textinput))
                    print(value)
                    placeholder=(value,)
                    #Start time to switch on
                    textinput="Change start time for on (HH:MM) :"
                    value=str(get_time(vars,str(convertsec(row["starttime"])),textinput))
                    print(value)
                    placeholder+=(value,)
                    #End time
                    textinput="Change end time for off (HH:MM) :"
                    value=str(get_time(vars,str(convertsec(row["endtime"])),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Operation, off, on or auto
                    textinput="Change io operation, 0=off, 1=on, 2=auto: "
                    value=str(get_operation(vars,str(row["operation"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #io device
                    list_devices(vars)
                    textinput="Change io device id number: "
                    value=str(get_device(vars,str(row["iodev"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #IO device port number
                    textinput="Change io device port number: "
                    value=str(get_int(vars,str(row["iodevnr"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    print "If using dependency, choose sensor or zone, not both"
                    subans=True
                    while subans:
                        print "Temperature zones"
                        list_zones(vars)
                        textinput="Change zone id: "
                        zoneid=get_int(vars,str(row["zone"]),textinput)
                        print "OR sensor"
                        list_sensors(vars)
                        textinput=" Or sensor id: "
                        sensorid=get_int(vars,str(row["sensor"]),textinput)
                        if zoneid and sensorid:
                            print "If using dependency, choose sensor or zone, not both"
                        else:
                            if zoneid:
                                value=zoneid
                                placeholder+=(zoneid,)
                                placeholder+=(str('0'),)
                            elif sensorid:
                                placeholder+=(str('0'),)
                                placeholder+=(sensorid,)
                            else:
                                placeholder+=(str('0'),)
                                placeholder+=(str('0'),)
                            subans=None
                    #Set logical
                    textinput="Is set value greater or smaller (> or <): "
                    value=str(get_logical(vars,str(row["logical"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    #Set Value
                    textinput="Change value: "
                    value=str(get_float(vars,str(row["value"]),textinput))
                    print(value)
                    placeholder+=(value,)

                    #Description
                    textinput="Change description: "
                    value=str(get_str(vars,str(row["description"]),textinput))
                    print(value)
                    placeholder+=(value,)
                    placeholder+=(str(ans),)
                    query="update timer SET name='%s', starttime='%s', endtime='%s', operation='%s',iodev='%s',iodevnr='%s',zone='%s', sensor='%s', logical='%s',value='%s', description='%s' where id='%s'"%(placeholder)
                    dbquery_update(vars,query) 
            except:
                print(ans)

loop=True
while loop:
    print("""
    1.Manage the devices
    2.Temperature zones
    3.Sensors
    4.Time control
    5.Exit
    """)
    ans=raw_input("Please select:")
    if ans=="1":
      print("\n Manage the devices")
      device_admin(vars)
    elif ans=="2":
      print("\n Temperature zones ")
      zone_admin(vars)
    elif ans=="3":
      print("\n Sensors")
      sensor_admin(vars)
    elif ans=="4":
      print("\n Time control")
      timer_admin(vars)
    elif ans=="5":
      print("\n Goodbye")
      loop = None
    else:
       print("\n Not Valid Choice Try again")
